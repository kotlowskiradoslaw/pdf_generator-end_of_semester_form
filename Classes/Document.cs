﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using System.Collections.Generic;

namespace pdf_form_generator
{
    public class Document
    {
        public static string schoolYear;
        public static int semester = 1;
        private readonly MigraDoc.DocumentObjectModel.Document pdf;
        public PageSetup DefaultPageSetup => pdf.DefaultPageSetup;
        public readonly List<Page> Pages = new List<Page>();

        public Document(string title, int pagesNumber)
        {
            pdf = new MigraDoc.DocumentObjectModel.Document() { Info = { Title = title } };
            for (int i = 0; i < pagesNumber; i++)
            {
                if (i == 0)
                {
                    _ = AddFirstPage();
                }
                else
                {
                    _ = AddPage();
                }
            }
        }

        private Page AddPage()
        {
            Section section = pdf.AddSection();
            Page page = new Page(section) { ParentDocument = this };
            Pages.Add(page);
            return page;
        }
        private Page AddFirstPage()
        {
            Section section = pdf.AddSection();
            Page page = new Page(section, true) { ParentDocument = this };
            Pages.Add(page);
            return page;
        }
        private void Save(string fileName)
        {
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true) { Document = pdf };
            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(fileName);
        }
        public string SaveWithErrorInfo(string fileName)
        {
            try
            {
                Save(fileName);
                return "OK";
            }
            catch (System.IO.IOException)
            {
                return "ERROR";
            }
        }
        public void SetHeaderFooter()
        {
            HeaderFooter header = Pages[0].Header;

            _ = header.AddParagraph(SectionHeaders.DocumentTitle);
            _ = header.AddParagraph(SectionHeaders.DocumentSubtitle);
        }
        public void WriteFirstPage()
        {
            Page page = Pages[0];

            page.WriteDate();
            page.WriteClass();
            page.WriteTeacher();
            page.WriteClassificationResults();
            if (semester == 1)
            {
                page.WriteStudentsNumberChangeReasons();
                page.WriteStudentsNonclassificationReasons();
            }
            page.WriteFailingGradesStudentsNumber();
            page.WriteFailingGradesStudentsList();
            page.WriteBehaviourGrades();
        }
        public void WriteSecondPage()
        {
            Page page = Pages[1];

            if (semester == 1)
            {
                page.WriteReprehensibleStudents();
            }
            page.WriteTitledStudents();
            page.WritePunishedStudents();
            page.WriteTeachingResultsStatistics();
            page.WriteAvaregeGrades();
            page.WriteAttendanceInformation();
            if (semester == 2)
            {
                page.WritePresentHoursInformation();
            }
            page.WriteSkippedHoursInformation();
            page.WriteConclusions();
            page.PreparePlaceForSigns();
        }
    }
}
