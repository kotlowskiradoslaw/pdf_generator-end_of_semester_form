﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Linq;

namespace pdf_form_generator
{
    public class Page
    {
        public static int FontSize = 10;

        public HeaderFooter Header => section.Headers.Primary;
        public readonly Section section;
        public Document ParentDocument;

        public Page(Section section, bool isFirstPage = false)
        {
            this.section = section;

            if (isFirstPage)
            {
                PrepareHeader();
            }
            PrepareFooter();
        }

        private Table AddTable()
        {
            return new Table(section.AddTable());
        }
        private Paragraph AddText()
        {
            return section.AddParagraph();
        }
        private Paragraph AddText(string text)
        {
            return section.AddParagraph(text);
        }
        private TextFrame AddTextFrame()
        {
            return section.AddTextFrame();
        }
        private void AddEmptyLine(int emptyLinesNumer = 1)
        {
            for (int i = 0; i < emptyLinesNumer; i++)
            {
                _ = AddText();
            }
        }
        private void PrepareFooter()
        {
            Paragraph footerText = section.Footers.Primary.AddParagraph();
            _ = footerText.AddPageField();
            footerText.Format.Alignment = ParagraphAlignment.Center;
        }
        private void PrepareHeader()
        {
            Header.Format.Alignment = ParagraphAlignment.Center;
            Header.Format.Font.Bold = true;
            Header.Format.Font.Color = Colors.DarkSlateGray;
        }
        private void FillBehaviourGradesStatistics(Table table)
        {
            table.AddRow().WriteText(new string[] {
                $"{SecondPageData.BehaviourGradesStatistics["6"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["6"] / SecondPageData.BehaviourGradesStatistics["Summary"])}",
                $"{SecondPageData.BehaviourGradesStatistics["5"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["5"] / SecondPageData.BehaviourGradesStatistics["Summary"])}",
                $"{SecondPageData.BehaviourGradesStatistics["4"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["4"] / SecondPageData.BehaviourGradesStatistics["Summary"])}",
                $"{SecondPageData.BehaviourGradesStatistics["3"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["3"] / SecondPageData.BehaviourGradesStatistics["Summary"])}",
                $"{SecondPageData.BehaviourGradesStatistics["2"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["2"] / SecondPageData.BehaviourGradesStatistics["Summary"])}",
                $"{SecondPageData.BehaviourGradesStatistics["1"]}",
                $"{Math.Round(100 * SecondPageData.BehaviourGradesStatistics["1"] / SecondPageData.BehaviourGradesStatistics["Summary"])}"
            });
        }
        public void WriteDate()
        {
            Paragraph line = AddText(SectionHeaders.DateText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(FirstPageData.Date);
            formattedText.Bold = false;
        }
        public void WriteClass()
        {
            Paragraph line = AddText(SectionHeaders.ClassText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(FirstPageData.Class);
            formattedText.Bold = false;
        }
        public void WriteTeacher()
        {
            Paragraph line = AddText(SectionHeaders.TeacherText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(FirstPageData.TeacherName);
            formattedText.Bold = false;

            AddEmptyLine(2);
        }
        public void WriteClassificationResults()
        {
            void specialHeaderFormatting1(Row r)
            {
                r.Cells[0].MergeDown = 1;
                r.Cells[0].MergeRight = 1;
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].MergeRight = Document.semester == 1 ? 5 : 7;
            }
            void specialHeaderFormatting2(Row r)
            {
                r.Cells[2].MergeRight = 1;
                r.Cells[4].MergeRight = 1;
                r.Cells[6].MergeRight = 1;
                if (Document.semester == 2)
                {
                    r.Cells[8].MergeRight = 1;
                }
            }

            Paragraph line = AddText(SectionHeaders.ClassificationResultsText);
            line.Format.Font.Bold = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: FirstPageData.ClassificationResultsTableColumnsSizes[Document.semester - 1]);

            int columnsNumber = TableTexts.ClassificationResultsTableSubheader.Length / 2;
            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(columnsNumber);
            row.WriteText(TableTexts.ClassificationResultsTableHeader);
            row.WriteEmptyRow(columnsNumber);
            row.SpecialFormatting(specialHeaderFormatting1);

            row = table.AddRow();
            row.MakeHeader(Colors.LightGray);
            row.TextAlignment = ParagraphAlignment.Center;
            row.WriteText(TableTexts.ClassificationResultsTableSubheader);
            row.SpecialFormatting(specialHeaderFormatting2);

            row = table.AddRow();
            row.WriteText(TableTexts.ClassificationResultsTableSubsubheader);

            row = table.AddRow();
            row.WriteText(TableTexts.ClassificationResultsTableText);
            table.KeepTogether();
            AddEmptyLine(2);
        }
        public void WriteStudentsNumberChangeReasons()
        {
            Paragraph line = AddText(SectionHeaders.StudentsNumberChangeReasonsText);
            line.Format.Font.Bold = true;

            _ = AddText(FirstPageData.ChangeOfStudentsNumberReasons);

            AddEmptyLine(2);
        }
        public void WriteStudentsNonclassificationReasons()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.StudentsNonclassificationReasonsText);
            line.Format.Font.Bold = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 5, 10 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(2);
            row.WriteText(TableTexts.StudentsNonclassificationReasonsTableHeader);
            row.WriteEmptyRow(2);
            row.SpecialFormatting(specialHeaderFormatting);

            foreach (SingledOutStudent unratedStudent in FirstPageData.UnratedStudents)
            {
                row = table.AddRow();
                row.WriteText(unratedStudent.ToArray());
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }
            if (FirstPageData.UnratedStudents.Count == 0)
            {
                row = table.AddEmptyRow();
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }

            AddEmptyLine(2);
        }
        public void WriteFailingGradesStudentsNumber()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.FailingGradesStudentsNumberText);
            line.Format.Font.Bold = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 6, 3, 3 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(3);
            row.WriteText(TableTexts.FailingGradesStudentsNumberTableHeader);
            row.WriteEmptyRow(3);
            row.SpecialFormatting(specialHeaderFormatting);

            row = table.AddRow();
            row.WriteText(TableTexts.FailingGradesStudentsNumberTableContent[0]);
            row = table.AddRow();
            row.WriteText(TableTexts.FailingGradesStudentsNumberTableContent[1]);
            row = table.AddRow();
            row.WriteText(TableTexts.FailingGradesStudentsNumberTableContent[2]);

            table.KeepTogether();
            AddEmptyLine(2);
        }
        public void WriteFailingGradesStudentsList()
        {
            void specialHeaderFormatting1(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            }
            void specialHeaderFormatting2(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].Format.Alignment = ParagraphAlignment.Justify;
            }

            Paragraph line = AddText(SectionHeaders.FailingGradesStudentsListText);
            line.Format.Font.Bold = true;

            if (Document.semester == 1)
            {
                FormattedText formattedText = line.AddFormattedText();
                _ = formattedText.AddText(SectionHeaders.FailingGradesStudentsListSubtext);
                formattedText.Bold = false;
            }

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 4.25, 3.75, 7 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(3);
            row.WriteText(TableTexts.FailingGradesStudentsListTableHeader);
            row.WriteEmptyRow(3);
            row.SpecialFormatting(specialHeaderFormatting1);

            foreach (FailingGradeStudent failingGradeStudent in FirstPageData.FailingGradeStudents)
            {
                row = table.AddRow();
                row.WriteText(failingGradeStudent.ToArray());
                row.SpecialFormatting(specialHeaderFormatting2);
            }
            if (FirstPageData.FailingGradeStudents.Count == 0)
            {
                row = table.AddEmptyRow();
                row.SpecialFormatting(specialHeaderFormatting2);
            }

            AddEmptyLine(2);
        }
        public void WriteBehaviourGrades()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].MergeRight = 1;
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].MergeRight = 1;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[4].MergeRight = 1;
                r.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[6].MergeRight = 1;
                r.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[8].MergeRight = 1;
                r.Cells[8].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[10].MergeRight = 1;
                r.Cells[10].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.BehaviourGradesText);
            line.Format.Font.Bold = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 1.35, 0.9, 1.75, 1.25, 1.35, 0.9, 1.35, 0.9, 1.75, 1.25, 1.35, 0.9 });

            TableRow row = table.AddRow();
            row.MakeHeader(color: Colors.Gray);
            row.WriteEmptyRow(10);
            row.WriteText(TableTexts.BehaviourGradesTableHeader);
            row.WriteEmptyRow(10);
            row.SpecialFormatting(specialHeaderFormatting);

            row = table.AddRow();
            row.MakeHeader(color: Colors.LightGray);
            row.WriteText(TableTexts.BehaviourGradesTableSubheader);

            SecondPageData.UpdateBehaviourGradesStatistics();
            FillBehaviourGradesStatistics(table);
            table.KeepTogether();
            AddEmptyLine(2);
        }
        public void WriteReprehensibleStudents()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.ReprehensibleStudentsText);
            line.Format.Font.Bold = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 5, 10 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(2);
            row.WriteText(TableTexts.ReprehensibleStudentsTableHeader);
            row.WriteEmptyRow(2);
            row.SpecialFormatting(specialHeaderFormatting);

            foreach (SingledOutStudent reprehensibleStudents in SecondPageData.ReprehensibleStudents)
            {
                row = table.AddRow();
                row.WriteText(reprehensibleStudents.ToArray());
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }
            if (SecondPageData.ReprehensibleStudents.Count == 0)
            {
                row = table.AddEmptyRow();
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }

            AddEmptyLine(2);
        }
        public void WriteTitledStudents()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.TitledStudentsText);
            line.Format.Font.Bold = true;
            line.Format.KeepTogether = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 5, 10 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(2);
            row.WriteText(TableTexts.TitledStudentsTableHeader);
            row.WriteEmptyRow(2);
            row.SpecialFormatting(specialHeaderFormatting);

            foreach (SingledOutStudent titledStudent in SecondPageData.TitledStudents)
            {
                row = table.AddRow();
                row.WriteText(titledStudent.ToArray());
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }
            if (SecondPageData.TitledStudents.Count == 0)
            {
                row = table.AddEmptyRow();
                row.SpecialFormatting(r => r.Cells[1].Format.Alignment = ParagraphAlignment.Justify);
            }

            AddEmptyLine(2);
        }
        public void WritePunishedStudents()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.PunishedStudentsText);
            line.Format.Font.Bold = true;
            line.Format.KeepTogether = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 4.1, 4.2, 6.7 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteEmptyRow(3);
            row.WriteText(TableTexts.PunishedStudentsTableHeader);
            row.WriteEmptyRow(3);
            row.SpecialFormatting(specialHeaderFormatting);

            foreach (PunishedStudent punishedStudent in SecondPageData.PunishedStudents)
            {
                if (Document.semester == 2)
                {
                    if (SecondPageData.AbsentHoursNumber == 0)
                    {
                        punishedStudent.Punishment += ", co stanowi 0%";
                    }
                    else
                    {
                        punishedStudent.Punishment += $", co stanowi {Math.Round(100 * decimal.Parse(punishedStudent.Punishment) / SecondPageData.AbsentHoursNumber)}%";
                    }
                }

                row = table.AddRow();
                row.WriteText(punishedStudent.ToArray());
                row.SpecialFormatting(specialHeaderFormatting);
                row.SpecialFormatting(r => r.Cells[2].Format.Alignment = ParagraphAlignment.Justify);
            }
            if (SecondPageData.PunishedStudents.Count == 0)
            {
                row = table.AddEmptyRow();
                row.SpecialFormatting(r => r.Cells[2].Format.Alignment = ParagraphAlignment.Justify);
            }

            AddEmptyLine(2);
        }
        public void WriteTeachingResultsStatistics()
        {
            void specialHeaderFormatting(Row r)
            {
                r.Height = 2 * FontSize;
                r.Format.Font.Size = FontSize;
                foreach (Cell cell in r.Cells) cell.VerticalAlignment = VerticalAlignment.Center;
            }

            Paragraph line = AddText(SectionHeaders.TeachingResultsStatisticsText);
            line.Format.Font.Bold = true;
            line.Format.KeepTogether = true;

            AddEmptyLine();

            Table table = AddTable();
            table.AddColumns(widths: new double[] { 3, 2, 2, 2, 2, 2, 2 });

            TableRow row = table.AddRow();
            row.MakeHeader(Colors.Gray);
            row.WriteText(TableTexts.TeachingResultsStatisticsTableHeader);
            row.SpecialFormatting(specialHeaderFormatting);

            row = table.AddRow();
            row.WriteText(TableTexts.TeachingResultsStatisticsTableContent[0]);
            row = table.AddRow();
            row.WriteText(TableTexts.TeachingResultsStatisticsTableContent[1]);

            table.KeepTogether();

            AddEmptyLine(2);
        }
        public void WriteAvaregeGrades()
        {
            Paragraph line = AddText(SectionHeaders.AvaregeGradesText);
            line.Format.Font.Bold = true;
            line.Format.KeepTogether = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(SecondPageData.SubjectGradesStatisticsList.Sum() == 0 ? "0" : $"{Math.Round(1 * SecondPageData.SubjectGradesStatisticsList.Select((v, i) => v * (6 - i)).Sum() / SecondPageData.SubjectGradesStatisticsList.Sum(), 2)}");
            formattedText.Bold = false;
        }
        public void WriteAttendanceInformation()
        {
            Paragraph line = AddText(SectionHeaders.AttendanceInformationText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText($"{SecondPageData.AttendancePercentage}%");
            formattedText.Bold = false;
        }
        public void WritePresentHoursInformation()
        {
            Paragraph line = AddText(SectionHeaders.WritePresentHoursInformationText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(SecondPageData.PresentHoursNumber.ToString());
            formattedText.Bold = false;
        }
        public void WriteSkippedHoursInformation()
        {
            Paragraph line = AddText(SectionHeaders.WriteSkippedHoursInformationText);
            line.Format.Font.Bold = true;

            FormattedText formattedText = line.AddFormattedText();
            _ = formattedText.AddText(SecondPageData.AbsentHoursNumber.ToString());
            formattedText.Bold = false;

            line = AddText();
            line.AddTab();
            _ = line.AddText(SectionHeaders.WriteSkippedHoursInformationSubtext);
            _ = line.AddText(SecondPageData.AbsentUnfairHoursNumber.ToString());

            AddEmptyLine(2);
        }
        public void WriteConclusions()
        {
            Paragraph line = AddText($"{ApplicationTexts.ConclusionsGroupBoxName}: ");
            line.Format.Font.Bold = true;

            line = AddText(SecondPageData.FinalConclusions);
            line.Format.Alignment = ParagraphAlignment.Justify;

            AddEmptyLine(2);
        }
        public void PreparePlaceForSigns()
        {
            MigraDoc.DocumentObjectModel.Tables.Table prepareTable()
            {
                TextFrame txtFrame = AddTextFrame();
                txtFrame.RelativeVertical = RelativeVertical.Page;
                txtFrame.Top = ShapePosition.Bottom;
                txtFrame.WrapFormat.DistanceBottom = ParentDocument.DefaultPageSetup.BottomMargin;

                MigraDoc.DocumentObjectModel.Tables.Table table = txtFrame.AddTable();
                table.Borders.Width = 0;
                table.KeepTogether = true;

                _ = table.AddColumn("16cm");
                _ = table.AddColumn("0cm");
                _ = table.AddRow();
                _ = table.AddRow();
                _ = table.AddRow();
                _ = table.AddRow();

                return table;
            }
            void prepareDots(Row row)
            {
                Paragraph paragraph = row.Cells[0].AddParagraph(TableTexts.PlaceForSignsDots);
                paragraph.AddTab();
                _ = paragraph.AddText(TableTexts.PlaceForSignsDots);
                paragraph.Format.ClearAll();
                _ = paragraph.Format.AddTabStop("15.7cm", TabAlignment.Right);
                _ = row.Cells[1].AddParagraph();
            }
            void prepareDescription(Row row)
            {
                Paragraph paragraph = row.Cells[0].AddParagraph(TableTexts.PlaceForSignsLocationAndDate);
                paragraph.AddTab();
                _ = paragraph.AddText(TableTexts.PlaceForSignsTeacherName);
                paragraph.Format.Font.Italic = true;
                paragraph.Format.ClearAll();
                _ = paragraph.Format.AddTabStop("15.7cm", TabAlignment.Right);
                _ = row.Cells[1].AddParagraph();
            }

            MigraDoc.DocumentObjectModel.Tables.Table t = prepareTable();
            prepareDots(t.AddRow());
            prepareDescription(t.AddRow());
        }
    }
}
