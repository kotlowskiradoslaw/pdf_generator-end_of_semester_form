﻿namespace pdf_form_generator
{
    public class FailingGradeStudent : SingledOutStudent
    {
        public string Subject;

        public FailingGradeStudent(string name, string subject, string reason) : base(name, reason)
        {
            Subject = subject;
        }

        public new string[] ToArray()
        {
            return new string[] { Name, Subject, Reason };
        }
    }
}
