﻿namespace pdf_form_generator
{
    public class PunishedStudent : SingledOutStudent
    {
        public string Punishment;

        public PunishedStudent(string name, string punishment, string reason) : base(name, reason)
        {
            Punishment = punishment;
        }

        public new string[] ToArray()
        {
            return new string[] { Name, Punishment, Reason };
        }
    }
}
