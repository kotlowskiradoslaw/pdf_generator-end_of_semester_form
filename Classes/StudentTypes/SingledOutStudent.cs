﻿namespace pdf_form_generator
{
    public class SingledOutStudent
    {
        public string Name;
        public string Reason;

        public SingledOutStudent(string name, string reason)
        {
            (Name, Reason) = (name, reason);
        }

        public string[] ToArray()
        {
            return new string[] { Name, Reason };
        }
    }
}
