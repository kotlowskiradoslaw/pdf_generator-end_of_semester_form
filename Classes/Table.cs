﻿using MigraDoc.DocumentObjectModel;
using System.Collections.Generic;

namespace pdf_form_generator
{
    public class Table
    {
        public MigraDoc.DocumentObjectModel.Tables.Table table;
        private readonly List<TableRow> Rows = new List<TableRow>();

        public Table(MigraDoc.DocumentObjectModel.Tables.Table table)
        {
            this.table = table;
            table.Style = "Table";
            table.Rows.Alignment = MigraDoc.DocumentObjectModel.Tables.RowAlignment.Center;
            table.Borders.Color = Colors.Black;
            table.Borders.Width = 0.25;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;
            table.Format.Alignment = ParagraphAlignment.Center;
        }

        public void AddColumns(double[] widths)
        {
            // column width in centimeters
            foreach (double width in widths) _ = table.AddColumn($"{width}cm");
        }
        public TableRow AddRow()
        {
            TableRow tRow = new TableRow(table.AddRow());
            Rows.Add(tRow);
            return tRow;
        }
        public void KeepTogether()
        {
            table.KeepTogether = true;
        }
        public TableRow AddEmptyRow()
        {
            TableRow row = AddRow();
            row.WriteText(new string[] { TableTexts.EmptyRowText });
            return row;
        }
    }
}
