﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;

namespace pdf_form_generator
{
    public class TableRow
    {
        private readonly Row row;
        public ParagraphAlignment TextAlignment;

        public TableRow(Row row)
        {
            this.row = row;
            TextAlignment = row.Format.Alignment;
        }

        public void WriteText(string[] texts)
        {
            for (int i = 0; i < texts.Length; i++)
            {
                _ = row.Cells[i].AddParagraph(texts[i]);
            }
        }
        public void WriteEmptyRow(int cellsNumber)
        {
            for (int i = 0; i < cellsNumber; i++)
            {
                _ = row.Cells[i].AddParagraph("");
            }
        }
        public void MakeHeader(Color color)
        {
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Shading.Color = color;
        }
        public void SpecialFormatting(System.Action<Row> formatting)
        {
            formatting(row);
        }
    }
}
