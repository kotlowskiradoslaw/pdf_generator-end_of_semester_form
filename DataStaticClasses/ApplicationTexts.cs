﻿namespace pdf_form_generator
{
    public static class ApplicationTexts
    {
        //
        // Form
        //
        public static readonly string FormName = "Generator karty wyników klasyfikacji - [nazwa szkoły] ";
        public static readonly string[] FormNameSuffixes = { "(1/4)", "(2/4)", "(3/4)", "(4/4)" };
        //
        // Buttons
        //
        public static readonly string ButtonPreviousFormText = "Wstecz";
        public static readonly string ButtonNextFormText = "Dalej";
        public static readonly string ButtonGeneratePdfText = "Generuj PDF";
        //
        // Common texts
        //
        public static readonly string StudentNameColumnText = "Imię i nazwisko";
        public static readonly string ReasonsColumnText = "Przyczyny";
        //
        // Initial data
        //
        public static readonly string InitialDataGroupBoxName = "Wstępne dane";
        public static readonly string[] SemesterRadioButtonsTexts = { "Semestr 1.", "Semestr 2." };
        public static readonly string DateLabelText = "Data:";
        public static readonly string ClassesLabelText = "Klasa:";
        public static readonly string[] ClassesTextsList = { "Klasa IV", "Klasa V", "Klasa VI", "Klasa VII", "Klasa VIII" };
        public static readonly string TeacherNameLabelText = "Wychowawca:";
        public static readonly string TeacherNameTextBoxDefaultText = "imię i nazwisko";
        //
        // Classification
        //
        public static readonly string ClassificationGroupBoxName = "Klasyfikacja";
        public static readonly string RatedStudentsNumberLabelText = "Klasyfikowani";
        public static readonly string UnratedStudentsNumberLabelText = "Nieklasyfikowani";
        public static readonly string[] NoFailingGradesStudentsNumberLabelText = { "Bez ocen ndst", "Z opinią z PPP" };
        public static readonly string ChangeStudentsNumberReasonsLabelText = "Przyczyny odejścia lub przybycia ucznia w trakcie roku szkolnego:";
        public static readonly string ChangeStudentsNumberReasonsTextBoxDefaultText = "brak";
        public static readonly string UnratedStudentsLabelText = "Uczniowie nieklasyfikowani";
        //
        // Failing grades students
        //
        public static string FailingGradesGroupBoxName => Document.semester == 1 ? "Wykaz uczniów z ocenami niedostatecznymi oraz propozycje działań:" : "Wykaz uczniów z ocenami niedostatecznymi:";
        public static readonly string FailingGradesStudentsSubjectColumnText = "Zajęcia edukacyjne";
        public static string FailingGradesStudentsActionPropositionsColumnText => Document.semester == 1 ? "Propozycje działań" : "Liczba ocen niedostatecznych (autouzupełnianie)";
        //
        // Failing grades students number
        public static readonly string FailingGradesCountGroupBoxName = "Liczba uczniów z ocenami niedostatecznymi";
        public static readonly string OneFailingGradeStudentsNumberLabelText = "Jedna ocena ndst:";
        public static readonly string TwoFailingGradesStudentsNumberLabelText = "Dwie oceny ndst:";
        public static readonly string ThreeAndMoreFailingGradesStudentsNumberLabelText = "Trzy lub więcej ocen ndst:";
        //
        // Behaviour grades
        //
        public static readonly string BehaviourGradesGroupBoxName = "Liczby ocen zachowania";
        public static readonly string[] BehaviourGradesLabelTexts = { "Wzorowe:", "Bardzo dobre:", "Dobre:", "Poprawne:", "Nieodpowiednie:", "Naganne:" };
        //
        // Reprehensible students
        //
        public static readonly string ReprehensibleStudentsGroupBoxName = "Uczniowie z oceną naganną";
        //
        // Titled students
        //
        public static readonly string TitledStudentsGroupBoxName = "Uczniowie wyróżnieni";
        public static string TitledStudentsReasonsColumnText => Document.semester == 1 ? ReasonsColumnText : "Średnia ocen";
        //
        // Punished students
        //
        public static string PunishedStudentsGroupBoxName => Document.semester == 1 ? "Uczniowie ukarani" : "Uczniowie z najniższą frekwencją";
        public static string PunishedStudentsPunishmentColumnText => Document.semester == 1 ? "Rodzaj kary" : "Godzin nieobecności";
        //
        // Education results statistics
        //
        public static readonly string EducationResultsStatisticsGroupBoxName = "Liczby wystawionych ocen";
        public static readonly string[] SubjectGradesLabelTexts = { "Celująca:", "Bardzo dobra:", "Dobra:", "Dostateczna:", "Dopuszczająca:", "Niedostateczna:" };
        //
        // Attendance statistics
        //
        public static readonly string AttendanceGroupBoxName = "Frekwencja klasy";
        public static readonly string HoursNumberLabelText = "Liczba godzin w miesiącu:";
        public static readonly string PresentHoursNumberLabelText = "obecności uczniów:";
        public static readonly string AbsentFairHoursNumberLabelText = "uspr. nieob. uczniów:";
        public static readonly string AbsentUnfairHoursNumberLabelText = "nieuspr. nieob. uczniów:";
        public static readonly string[] MonthsLabelsText = {
            "Wrzesień:", "Październik:", "Listopad:", "Grudzień:", "Styczeń:",
            "Luty:", "Marzec:", "Kwiecień:", "Maj:", "Czerwiec:",
        };
        //
        // Conclusions
        //
        public static string ConclusionsGroupBoxName => new string[] {
            "Wnioski do pracy wychowawczej ze swoją klasą na II semestr (półrocze/okres)",
            "Krótka charakterystyka klasy"
        }[Document.semester - 1];
        //
        // Folder browser dialog
        //
        public static readonly string FolderBrowserDialogText = "Wybierz lokalizację generowanego pliku.";
        //
        // Result file
        //
        public static string FileName => $"Wyniki klasyfikacji {(Document.semester == 1 ? "semestralnej" : "rocznej")}, {FirstPageData.Class}, {Document.schoolYear.Replace("/", "-")}";
        public static readonly string DocumentTitle = "Karta semestralna";
        //
        // Error texts
        //
        public static readonly string FileOpenedErrorText = "Zamknij plik pdf.";
    }
}
