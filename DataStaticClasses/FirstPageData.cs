﻿using System.Collections.Generic;

namespace pdf_form_generator
{
    public static class FirstPageData
    {
        public static string Date = "";
        public static string Class = "";
        public static string TeacherName = "";
        public static decimal RatedStudentsNumber = 1;
        public static decimal UnratedStudentsNumber = 0;
        public static decimal NoFailingGradeStudentsNumber = 0;
        public static decimal TitledStudentsNumber = 0;
        public static string ChangeOfStudentsNumberReasons = "";
        public static List<SingledOutStudent> UnratedStudents = new List<SingledOutStudent>();
        public static decimal OneFailingGradeStudentsNumber = 1;
        public static decimal TwoFailingGradesStudentsNumber = 0;
        public static decimal ThreeAndMoreFailingGradesStudentsNumber = 0;
        public static List<FailingGradeStudent> FailingGradeStudents = new List<FailingGradeStudent>();

        public static readonly double[][] ClassificationResultsTableColumnsSizes = {
            new double[] { 2.1, 2.1, 2.1, 1.5, 2.1, 1.5, 2.1, 1.5 },
            new double[] { 1.9, 1.8, 1.5, 1.1, 1.8, 1.2, 1.5, 1.2, 1.8, 1.2 }
        };
    }
}
