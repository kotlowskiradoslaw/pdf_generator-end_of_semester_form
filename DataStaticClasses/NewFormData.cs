﻿using System.Drawing;
using System.Windows.Forms;

namespace pdf_form_generator
{
    public static class NewFormData
    {
        //
        // Form settings
        //
        public static readonly SizeF AutoScaleDimensions = new SizeF(6F, 13F);
        public static readonly AutoScaleMode AutoScaleMode = AutoScaleMode.Font;
        public static readonly bool AutoSize = true;
        public static readonly Color BackColor = SystemColors.Control;
        public static readonly Size ClientSize = new Size(784, 561);
        public static readonly FormBorderStyle FormBorderStyle = FormBorderStyle.FixedSingle;
        public static readonly Padding Margin = new Padding(2);
        public static readonly bool MaximizeBox = false;
        public static readonly string Name = "PDFGeneratorForm";
        public static readonly FormStartPosition StartPosition = FormStartPosition.CenterScreen;
        public static readonly string Text = ApplicationTexts.FormName;
    }
}
