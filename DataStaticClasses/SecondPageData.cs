﻿using System.Collections.Generic;
using System.Linq;

namespace pdf_form_generator
{
    public static class SecondPageData
    {
        public static Dictionary<string, decimal> BehaviourGradesStatistics = new Dictionary<string, decimal>();
        public static List<SingledOutStudent> ReprehensibleStudents = new List<SingledOutStudent>();
        public static List<SingledOutStudent> TitledStudents = new List<SingledOutStudent>();
        public static List<PunishedStudent> PunishedStudents = new List<PunishedStudent>();
        public static Dictionary<string, decimal> SubjectGradesStatistics = new Dictionary<string, decimal>();
        public static decimal[] SubjectGradesStatisticsList => SubjectGradesStatistics.Select(gradeStatistics => gradeStatistics.Value).ToArray();
        public static decimal AbsentHoursNumber;
        public static decimal AbsentUnfairHoursNumber;
        public static decimal PresentHoursNumber => Attendance[0][2].Sum() + (Attendance[1] == null ? 0 : Attendance[1][2].Sum());
        public static decimal AllHoursNumber;
        public static decimal[][][] Attendance = new decimal[2][][];
        public static decimal AttendancePercentage;
        public static void SetAttendancePercentage(decimal presentHoursNumber)
        {
            AllHoursNumber = presentHoursNumber + AbsentHoursNumber;
            if (AllHoursNumber == 0)
            {
                AttendancePercentage = 0;
            }
            else
            {
                AttendancePercentage = System.Math.Round(100 * presentHoursNumber / AllHoursNumber);
            }
        }
        public static string FinalConclusions = "";
        public static void UpdateBehaviourGradesStatistics()
        {
            BehaviourGradesStatistics["Summary"] = BehaviourGradesStatistics.Sum((kv) => kv.Value);
            if (BehaviourGradesStatistics["Summary"] == 0)
            {
                BehaviourGradesStatistics["Summary"] = short.MaxValue;
            }
        }
    }
}
