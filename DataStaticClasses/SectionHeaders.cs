﻿namespace pdf_form_generator
{
    public static class SectionHeaders
    {
        public static string DocumentTitle => Document.semester == 1 ?
            "Wyniki klasyfikacji śródrocznej klas IV - VIII" :
            $"Sprawozdanie wychowawcy z klasyfikacji rocznej w roku szkolnym {Document.schoolYear}";
        public static string DocumentSubtitle => Document.semester == 1 ?
            $"I semestr roku szkolnego {Document.schoolYear} w Szkole Podstawowej [nazwa]" :
            "Załącznik nr 2a do Regulaminu Rady Pedagogicznej\nSzkoły Podstawowej [nazwa]";
        public static readonly string DateText = "Data: ";
        public static readonly string ClassText = "Klasa: ";
        public static readonly string TeacherText = "Wychowawca: ";
        public static readonly string ClassificationResultsText = "Wyniki klasyfikacji";
        public static readonly string StudentsNumberChangeReasonsText = "Przyczyny odejścia lub przybycia ucznia w trakcie roku szkolnego";
        public static readonly string StudentsNonclassificationReasonsText = "Uczniowie nieklasyfikowani";
        public static readonly string FailingGradesStudentsNumberText = "Liczba uczniów z ocenami niedostatecznymi";
        public static readonly string FailingGradesStudentsListText = "Wykaz uczniów z ocenami niedostatecznymi";
        public static readonly string FailingGradesStudentsListSubtext = " i propozycje działań mających na celu umożliwienie uczniom uzupełnienia braków";
        public static readonly string BehaviourGradesText = "Oceny zachowania";
        public static readonly string ReprehensibleStudentsText = "Uczniowie z oceną naganną";
        public static readonly string TitledStudentsText = "Uczniowie wyróżnieni";
        public static string PunishedStudentsText => Document.semester == 1 ? "Uczniowie ukarani" : "Uczniowie z najniższą frekwencją";
        public static readonly string TeachingResultsStatisticsText = "Statystyka wyników nauczania";
        public static readonly string AvaregeGradesText = "Średnia ocen w klasie: ";
        public static readonly string AttendanceInformationText = "Średnia frekwencja klasy: ";
        public static readonly string WritePresentHoursInformationText = "Liczba godzin obecności: ";
        public static readonly string WriteSkippedHoursInformationText = "Liczba godzin opuszczonych: ";
        public static readonly string WriteSkippedHoursInformationSubtext = "w tym nieusprawiedliwionych: ";
    }
}
