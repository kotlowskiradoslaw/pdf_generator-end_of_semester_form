﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace pdf_form_generator
{
    public static class TableTexts
    {
        public static readonly string EmptyRowText = "brak";

        public static readonly string[] ClassificationResultsTableHeader = { "Liczba uczniów\nwg stanu w dniu", "", "Uczniowie" };
        public static string[] ClassificationResultsTableSubheader => Document.semester == 1 ?
            new string[] { "", "", "Klasyfikowani", "", "Nieklasyfikowani", "", "Bez ocen ndst" } :
            new string[] { "", "", "Klasyfikowani", "", "Nieklasyfikowani", "", "Z opinią z PPP", "", "Z wyróżnieniem" };
        public static string[] ClassificationResultsTableSubsubheader => Document.semester == 1 ?
            new string[] { FirstPageData.Date, "klasyfikacji", "liczba", "%", "liczba", "%", "liczba", "%" } :
            new string[] { FirstPageData.Date, "klasyfikacji", "liczba", "%", "liczba", "%", "liczba", "%", "liczba", "%" };
        public static string[] ClassificationResultsTableText => GenerateClassificationResultsTableText();

        public static readonly string[] StudentsNonclassificationReasonsTableHeader = { "Imię i nazwisko ucznia nieklasyfikowanego", "Przyczyny" };

        public static readonly string[] FailingGradesStudentsNumberTableHeader = { "Liczba ocen niedostatecznych", "Liczba osób", "Co stanowi (%)" };
        public static string[][] FailingGradesStudentsNumberTableContent => GenerateFailingGradesStudentsNumberTableContent();
        public static string[] FailingGradesStudentsListTableHeader => new string[] { "Imię i nazwisko ucznia", "Zajęcia edukacyjne", Document.semester == 1 ? "Propozycje działań" : "Liczba ocen niedostatecznych" };

        public static readonly string[] BehaviourGradesTableHeader = { "Wzorowe", "", "Bardzo dobre", "", "Dobre", "", "Poprawne", "", "Nieodpowiednie", "", "Naganne", "" };
        public static readonly string[] BehaviourGradesTableSubheader = { "Liczba", "%", "Liczba", "%", "Liczba", "%", "Liczba", "%", "Liczba", "%", "Liczba", "%" };

        public static readonly string[] ReprehensibleStudentsTableHeader = { "Imię i nazwisko ucznia", "Przyczyny" };

        public static readonly string[] TitledStudentsTableHeader = { "Imię i nazwisko ucznia", Document.semester == 1 ? "Przyczyny" : "Średnia ocen" };

        public static string[] PunishedStudentsTableHeader => new string[] { "Imię i nazwisko ucznia", Document.semester == 1 ? "Rodzaj kary" : "Godzin nieobecności", "Przyczyny" };

        public static readonly string[] TeachingResultsStatisticsTableHeader = { "Oceny", "cel", "bdb", "db", "dst", "dop", "ndst" };
        public static string[][] TeachingResultsStatisticsTableContent => GenerateTeachingResultsStatisticsTableContent();

        public static readonly string PlaceForSignsDots = "............................................................";
        public static readonly string PlaceForSignsLocationAndDate = "(miejscowość, data)";
        public static readonly string PlaceForSignsTeacherName = "(podpis wychowawcy)";

        private static string[] GenerateClassificationResultsTableText()
        {
            decimal suma = FirstPageData.RatedStudentsNumber + FirstPageData.UnratedStudentsNumber;
            if (suma == 0)
            {
                suma = short.MaxValue;
            }
            List<string> result = new List<string> {
                $"{(suma == short.MaxValue ? 0 : suma)}",
                "",
                $"{FirstPageData.RatedStudentsNumber}", $"{Math.Round(100 * FirstPageData.RatedStudentsNumber / suma)}",
                $"{FirstPageData.UnratedStudentsNumber}", $"{Math.Round(100 * FirstPageData.UnratedStudentsNumber / suma)}",
                $"{FirstPageData.NoFailingGradeStudentsNumber}", $"{Math.Round(100 * FirstPageData.NoFailingGradeStudentsNumber / suma)}"
            };
            if (Document.semester == 2)
            {
                result.Add($"{FirstPageData.TitledStudentsNumber}");
                result.Add($"{Math.Round(100 * FirstPageData.TitledStudentsNumber / suma)}");
            }
            return result.ToArray();
        }

        private static string[][] GenerateFailingGradesStudentsNumberTableContent()
        {
            decimal suma = FirstPageData.OneFailingGradeStudentsNumber + FirstPageData.TwoFailingGradesStudentsNumber + FirstPageData.ThreeAndMoreFailingGradesStudentsNumber;
            if (suma == 0)
            {
                suma = short.MaxValue;
            }
            string[][] result = new string[3][];
            result[0] = new string[] { "Jedna", $"{FirstPageData.OneFailingGradeStudentsNumber}", $"{Math.Round(100 * FirstPageData.OneFailingGradeStudentsNumber / suma)}" };
            result[1] = new string[] { "Dwie", $"{FirstPageData.TwoFailingGradesStudentsNumber}", $"{Math.Round(100 * FirstPageData.TwoFailingGradesStudentsNumber / suma)}" };
            result[2] = new string[] { "Trzy i więcej", $"{FirstPageData.ThreeAndMoreFailingGradesStudentsNumber}", $"{Math.Round(100 * FirstPageData.ThreeAndMoreFailingGradesStudentsNumber / suma)}" };
            return result;
        }

        private static string[][] GenerateTeachingResultsStatisticsTableContent()
        {
            string[][] result = new string[2][];
            result[0] = new string[] { "Liczba" }.Concat(SecondPageData.SubjectGradesStatisticsList.Select(i => $"{i}")).ToArray();
            decimal sum = SecondPageData.SubjectGradesStatisticsList.Sum();
            if (sum == 0)
            {
                sum = short.MaxValue;
            }
            result[1] = new string[] { "%" }.Concat(SecondPageData.SubjectGradesStatisticsList.Select(i => $"{Math.Round(100 * i / sum)}%")).ToArray();
            return result;
        }
    }
}
