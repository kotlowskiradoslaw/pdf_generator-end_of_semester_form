﻿namespace pdf_form_generator
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox_SemesterNumber = new System.Windows.Forms.GroupBox();
            this.radioButton_Semester2 = new System.Windows.Forms.RadioButton();
            this.radioButton_Semester1 = new System.Windows.Forms.RadioButton();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.textBox_TeacherName = new System.Windows.Forms.TextBox();
            this.groupBox_InitialData = new System.Windows.Forms.GroupBox();
            this.label_Date = new System.Windows.Forms.Label();
            this.comboBox_Classes = new System.Windows.Forms.ComboBox();
            this.label_TeacherName = new System.Windows.Forms.Label();
            this.label_Classes = new System.Windows.Forms.Label();
            this.groupBox_Classification = new System.Windows.Forms.GroupBox();
            this.dataGridView_UnratedStudents = new System.Windows.Forms.DataGridView();
            this.Column_UnratedStudents_StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_UnratedStudents_Reasons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label_UnratedStudents = new System.Windows.Forms.Label();
            this.textBox_ChangeStudentsNumberReasons = new System.Windows.Forms.TextBox();
            this.label_ChangeStudentsNumberReasons = new System.Windows.Forms.Label();
            this.numericUpDown_NoFailingGradesStudentsNumber = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_UnratedStudentsNumber = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_RatedStudentsNumber = new System.Windows.Forms.NumericUpDown();
            this.label_NoFailingGradesStudentsNumber = new System.Windows.Forms.Label();
            this.label_UnratedStudentsNumber = new System.Windows.Forms.Label();
            this.label_RatedStudentsNumber = new System.Windows.Forms.Label();
            this.groupBox_SemesterNumber.SuspendLayout();
            this.groupBox_InitialData.SuspendLayout();
            this.groupBox_Classification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnratedStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NoFailingGradesStudentsNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_UnratedStudentsNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RatedStudentsNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_InitialData
            // 
            this.groupBox_InitialData.Controls.Add(this.label_Date);
            this.groupBox_InitialData.Controls.Add(this.comboBox_Classes);
            this.groupBox_InitialData.Controls.Add(this.label_TeacherName);
            this.groupBox_InitialData.Controls.Add(this.label_Classes);
            this.groupBox_InitialData.Controls.Add(this.groupBox_SemesterNumber);
            this.groupBox_InitialData.Controls.Add(this.textBox_TeacherName);
            this.groupBox_InitialData.Controls.Add(this.dateTimePicker);
            this.groupBox_InitialData.Location = new System.Drawing.Point(12, 12);
            this.groupBox_InitialData.Name = "groupBox_InitialData";
            this.groupBox_InitialData.Size = new System.Drawing.Size(760, 73);
            this.groupBox_InitialData.TabIndex = 0;
            this.groupBox_InitialData.TabStop = false;
            this.groupBox_InitialData.Text = "groupBox_TitledStudents";
            // 
            // groupBox_SemesterNumber
            // 
            this.groupBox_SemesterNumber.Controls.Add(this.radioButton_Semester2);
            this.groupBox_SemesterNumber.Controls.Add(this.radioButton_Semester1);
            this.groupBox_SemesterNumber.Location = new System.Drawing.Point(6, 19);
            this.groupBox_SemesterNumber.Name = "groupBox_SemesterNumber";
            this.groupBox_SemesterNumber.Size = new System.Drawing.Size(188, 45);
            this.groupBox_SemesterNumber.TabIndex = 0;
            this.groupBox_SemesterNumber.TabStop = false;
            // 
            // radioButton_Semester1
            // 
            this.radioButton_Semester1.AutoSize = true;
            this.radioButton_Semester1.Checked = true;
            this.radioButton_Semester1.CheckedChanged += new System.EventHandler(SemesterChanged);
            this.radioButton_Semester1.Location = new System.Drawing.Point(6, 19);
            this.radioButton_Semester1.Name = "radioButton_Semester1";
            this.radioButton_Semester1.Size = new System.Drawing.Size(85, 17);
            this.radioButton_Semester1.TabIndex = 1;
            this.radioButton_Semester1.TabStop = true;
            this.radioButton_Semester1.Text = "radioButton1";
            this.radioButton_Semester1.UseVisualStyleBackColor = true;
            // 
            // radioButton_Semester2
            // 
            this.radioButton_Semester2.AutoSize = true;
            this.radioButton_Semester2.CheckedChanged += new System.EventHandler(SemesterChanged);
            this.radioButton_Semester2.Location = new System.Drawing.Point(94, 19);
            this.radioButton_Semester2.Name = "radioButton_Semester2";
            this.radioButton_Semester2.Size = new System.Drawing.Size(85, 17);
            this.radioButton_Semester2.TabIndex = 2;
            this.radioButton_Semester2.Text = "radioButton2";
            this.radioButton_Semester2.UseVisualStyleBackColor = true;
            // 
            // label_Date
            // 
            this.label_Date.Location = new System.Drawing.Point(200, 36);
            this.label_Date.Name = "label_Date";
            this.label_Date.Size = new System.Drawing.Size(37, 21);
            this.label_Date.Text = "label1";
            this.label_Date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker.Location = new System.Drawing.Point(243, 37);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(78, 20);
            this.dateTimePicker.TabIndex = 3;
            // 
            // label_Classes
            // 
            this.label_Classes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Classes.Location = new System.Drawing.Point(327, 36);
            this.label_Classes.Name = "label_Classes";
            this.label_Classes.Size = new System.Drawing.Size(37, 21);
            this.label_Classes.Text = "label1";
            this.label_Classes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_Classes
            // 
            this.comboBox_Classes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Classes.FormattingEnabled = true;
            this.comboBox_Classes.Location = new System.Drawing.Point(370, 37);
            this.comboBox_Classes.Name = "comboBox_Classes";
            this.comboBox_Classes.Size = new System.Drawing.Size(98, 21);
            this.comboBox_Classes.TabIndex = 4;
            // 
            // label_TeacherName
            // 
            this.label_TeacherName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_TeacherName.Location = new System.Drawing.Point(474, 36);
            this.label_TeacherName.Name = "label_TeacherName";
            this.label_TeacherName.Size = new System.Drawing.Size(80, 20);
            this.label_TeacherName.Text = "label1";
            this.label_TeacherName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox_TeacherName
            // 
            this.textBox_TeacherName.Location = new System.Drawing.Point(560, 37);
            this.textBox_TeacherName.Name = "textBox_TeacherName";
            this.textBox_TeacherName.Size = new System.Drawing.Size(194, 20);
            this.textBox_TeacherName.TabIndex = 5;
            // 
            // groupBox_Classification
            // 
            this.groupBox_Classification.Controls.Add(this.dataGridView_UnratedStudents);
            this.groupBox_Classification.Controls.Add(this.label_UnratedStudents);
            this.groupBox_Classification.Controls.Add(this.textBox_ChangeStudentsNumberReasons);
            this.groupBox_Classification.Controls.Add(this.label_ChangeStudentsNumberReasons);
            this.groupBox_Classification.Controls.Add(this.numericUpDown_NoFailingGradesStudentsNumber);
            this.groupBox_Classification.Controls.Add(this.numericUpDown_UnratedStudentsNumber);
            this.groupBox_Classification.Controls.Add(this.numericUpDown_RatedStudentsNumber);
            this.groupBox_Classification.Controls.Add(this.label_NoFailingGradesStudentsNumber);
            this.groupBox_Classification.Controls.Add(this.label_UnratedStudentsNumber);
            this.groupBox_Classification.Controls.Add(this.label_RatedStudentsNumber);
            this.groupBox_Classification.Location = new System.Drawing.Point(12, 90);
            this.groupBox_Classification.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox_Classification.Name = "groupBox_Classification";
            this.groupBox_Classification.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox_Classification.Size = new System.Drawing.Size(760, 435);
            this.groupBox_Classification.TabIndex = 1;
            this.groupBox_Classification.TabStop = false;
            this.groupBox_Classification.Text = "groupBox_TitledStudents";
            // 
            // dataGridView_UnratedStudents
            // 
            this.dataGridView_UnratedStudents.AllowUserToResizeColumns = false;
            this.dataGridView_UnratedStudents.AllowUserToResizeRows = false;
            this.dataGridView_UnratedStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_UnratedStudents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_UnratedStudents.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_UnratedStudents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridView_UnratedStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_UnratedStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_UnratedStudents_StudentName,
            this.Column_UnratedStudents_Reasons});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_UnratedStudents.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridView_UnratedStudents.Location = new System.Drawing.Point(23, 204);
            this.dataGridView_UnratedStudents.Name = "dataGridView_UnratedStudents";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_UnratedStudents.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView_UnratedStudents.RowHeadersVisible = false;
            this.dataGridView_UnratedStudents.RowHeadersWidth = 65;
            this.dataGridView_UnratedStudents.Size = new System.Drawing.Size(731, 226);
            this.dataGridView_UnratedStudents.TabIndex = 10;
            // 
            // Column_UnratedStudents_StudentName
            // 
            this.Column_UnratedStudents_StudentName.HeaderText = "";
            this.Column_UnratedStudents_StudentName.Name = "Column_UnratedStudents_StudentName";
            this.Column_UnratedStudents_StudentName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_UnratedStudents_StudentName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_UnratedStudents_StudentName.Width = 180;
            // 
            // Column_UnratedStudents_Reasons
            // 
            this.Column_UnratedStudents_Reasons.HeaderText = "";
            this.Column_UnratedStudents_Reasons.Name = "Column_UnratedStudents_Reasons";
            this.Column_UnratedStudents_Reasons.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_UnratedStudents_Reasons.Width = 530;
            // 
            // label_UnratedStudents
            // 
            this.label_UnratedStudents.AutoSize = true;
            this.label_UnratedStudents.Location = new System.Drawing.Point(22, 188);
            this.label_UnratedStudents.Name = "label_UnratedStudents";
            this.label_UnratedStudents.Size = new System.Drawing.Size(35, 13);
            this.label_UnratedStudents.Text = "label1";
            // 
            // textBox_ChangeStudentsNumberReasons
            // 
            this.textBox_ChangeStudentsNumberReasons.Location = new System.Drawing.Point(100, 37);
            this.textBox_ChangeStudentsNumberReasons.Multiline = true;
            this.textBox_ChangeStudentsNumberReasons.Name = "textBox_ChangeStudentsNumberReasons";
            this.textBox_ChangeStudentsNumberReasons.Size = new System.Drawing.Size(654, 128);
            this.textBox_ChangeStudentsNumberReasons.TabIndex = 9;
            // 
            // label_ChangeStudentsNumberReasons
            // 
            this.label_ChangeStudentsNumberReasons.AutoSize = true;
            this.label_ChangeStudentsNumberReasons.Location = new System.Drawing.Point(99, 21);
            this.label_ChangeStudentsNumberReasons.Name = "label_ChangeStudentsNumberReasons";
            this.label_ChangeStudentsNumberReasons.Size = new System.Drawing.Size(35, 13);
            this.label_ChangeStudentsNumberReasons.Text = "label1";
            // 
            // numericUpDown_NoFailingGradesStudentsNumber
            // 
            this.numericUpDown_NoFailingGradesStudentsNumber.Location = new System.Drawing.Point(25, 145);
            this.numericUpDown_NoFailingGradesStudentsNumber.Name = "numericUpDown_NoFailingGradesStudentsNumber";
            this.numericUpDown_NoFailingGradesStudentsNumber.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_NoFailingGradesStudentsNumber.TabIndex = 8;
            this.numericUpDown_NoFailingGradesStudentsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown_UnratedStudentsNumber
            // 
            this.numericUpDown_UnratedStudentsNumber.Location = new System.Drawing.Point(25, 94);
            this.numericUpDown_UnratedStudentsNumber.Name = "numericUpDown_UnratedStudentsNumber";
            this.numericUpDown_UnratedStudentsNumber.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_UnratedStudentsNumber.TabIndex = 7;
            this.numericUpDown_UnratedStudentsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown_RatedStudentsNumber
            // 
            this.numericUpDown_RatedStudentsNumber.Location = new System.Drawing.Point(25, 43);
            this.numericUpDown_RatedStudentsNumber.Name = "numericUpDown_RatedStudentsNumber";
            this.numericUpDown_RatedStudentsNumber.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_RatedStudentsNumber.TabIndex = 6;
            this.numericUpDown_RatedStudentsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_NoFailingGradesStudentsNumber
            // 
            this.label_NoFailingGradesStudentsNumber.Location = new System.Drawing.Point(5, 117);
            this.label_NoFailingGradesStudentsNumber.Name = "label_NoFailingGradesStudentsNumber";
            this.label_NoFailingGradesStudentsNumber.Size = new System.Drawing.Size(100, 25);
            this.label_NoFailingGradesStudentsNumber.Text = "label3";
            this.label_NoFailingGradesStudentsNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UnratedStudentsNumber
            // 
            this.label_UnratedStudentsNumber.Location = new System.Drawing.Point(5, 66);
            this.label_UnratedStudentsNumber.Name = "label_UnratedStudentsNumber";
            this.label_UnratedStudentsNumber.Size = new System.Drawing.Size(100, 25);
            this.label_UnratedStudentsNumber.Text = "label2";
            this.label_UnratedStudentsNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_RatedStudentsNumber
            // 
            this.label_RatedStudentsNumber.Location = new System.Drawing.Point(5, 15);
            this.label_RatedStudentsNumber.Name = "label_RatedStudentsNumber";
            this.label_RatedStudentsNumber.Size = new System.Drawing.Size(100, 25);
            this.label_RatedStudentsNumber.Text = "label1";
            this.label_RatedStudentsNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.Controls.Add(this.groupBox_Classification);
            this.Controls.Add(this.groupBox_InitialData);
            this.groupBox_SemesterNumber.ResumeLayout(false);
            this.groupBox_SemesterNumber.PerformLayout();
            this.groupBox_InitialData.ResumeLayout(false);
            this.groupBox_InitialData.PerformLayout();
            this.groupBox_Classification.ResumeLayout(false);
            this.groupBox_Classification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UnratedStudents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NoFailingGradesStudentsNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_UnratedStudentsNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RatedStudentsNumber)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_SemesterNumber;
        private System.Windows.Forms.RadioButton radioButton_Semester2;
        private System.Windows.Forms.RadioButton radioButton_Semester1;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TextBox textBox_TeacherName;
        private System.Windows.Forms.GroupBox groupBox_InitialData;
        private System.Windows.Forms.Label label_TeacherName;
        private System.Windows.Forms.ComboBox comboBox_Classes;
        private System.Windows.Forms.GroupBox groupBox_Classification;
        private System.Windows.Forms.NumericUpDown numericUpDown_NoFailingGradesStudentsNumber;
        private System.Windows.Forms.NumericUpDown numericUpDown_UnratedStudentsNumber;
        private System.Windows.Forms.NumericUpDown numericUpDown_RatedStudentsNumber;
        private System.Windows.Forms.Label label_NoFailingGradesStudentsNumber;
        private System.Windows.Forms.Label label_UnratedStudentsNumber;
        private System.Windows.Forms.Label label_RatedStudentsNumber;
        private System.Windows.Forms.TextBox textBox_ChangeStudentsNumberReasons;
        private System.Windows.Forms.Label label_ChangeStudentsNumberReasons;
        private System.Windows.Forms.Label label_UnratedStudents;
        private System.Windows.Forms.DataGridView dataGridView_UnratedStudents;
        private System.Windows.Forms.Label label_Date;
        private System.Windows.Forms.Label label_Classes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_UnratedStudents_StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_UnratedStudents_Reasons;
    }
}

