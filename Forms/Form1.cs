﻿using System;
using System.Windows.Forms;

namespace pdf_form_generator
{
    public partial class Form1 : NewForm
    {
        public Form1() : base()
        {
            InitializeComponent();
            WriteTexts();

            ButtonNextForm.Click += new EventHandler(HandleWrongStudentsNumber);
        }

        private void WriteTexts()
        {
            Text += ApplicationTexts.FormNameSuffixes[0];

            groupBox_InitialData.Text = ApplicationTexts.InitialDataGroupBoxName;
            radioButton_Semester1.Text = ApplicationTexts.SemesterRadioButtonsTexts[0];
            radioButton_Semester2.Text = ApplicationTexts.SemesterRadioButtonsTexts[1];
            label_Date.Text = ApplicationTexts.DateLabelText;
            label_TeacherName.Text = ApplicationTexts.TeacherNameLabelText;
            textBox_TeacherName.Text = ApplicationTexts.TeacherNameTextBoxDefaultText;
            label_Classes.Text = ApplicationTexts.ClassesLabelText;
            comboBox_Classes.DataSource = ApplicationTexts.ClassesTextsList;

            groupBox_Classification.Text = ApplicationTexts.ClassificationGroupBoxName;
            label_RatedStudentsNumber.Text = ApplicationTexts.RatedStudentsNumberLabelText;
            label_UnratedStudentsNumber.Text = ApplicationTexts.UnratedStudentsNumberLabelText;
            label_NoFailingGradesStudentsNumber.Text = ApplicationTexts.NoFailingGradesStudentsNumberLabelText[0];
            label_ChangeStudentsNumberReasons.Text = ApplicationTexts.ChangeStudentsNumberReasonsLabelText;
            textBox_ChangeStudentsNumberReasons.Text = ApplicationTexts.ChangeStudentsNumberReasonsTextBoxDefaultText;
            label_UnratedStudents.Text = ApplicationTexts.UnratedStudentsLabelText;
            Column_UnratedStudents_StudentName.HeaderText = ApplicationTexts.StudentNameColumnText;
            Column_UnratedStudents_Reasons.HeaderText = ApplicationTexts.ReasonsColumnText;
        }
        protected override void GatherData()
        {
            DateTime selectedDate = dateTimePicker.Value;
            if (selectedDate.Month < 8)
            {
                Document.schoolYear = $"{selectedDate.Year - 1}/{selectedDate.Year}";
            }
            else
            {
                Document.schoolYear = $"{selectedDate.Year}/{selectedDate.Year + 1}";
            }

            FirstPageData.Date = selectedDate.ToShortDateString();
            FirstPageData.Class = comboBox_Classes.SelectedItem.ToString();
            FirstPageData.TeacherName = textBox_TeacherName.Text;

            FirstPageData.RatedStudentsNumber = numericUpDown_RatedStudentsNumber.Value;
            FirstPageData.UnratedStudentsNumber = numericUpDown_UnratedStudentsNumber.Value;
            FirstPageData.NoFailingGradeStudentsNumber = numericUpDown_NoFailingGradesStudentsNumber.Value;

            FirstPageData.ChangeOfStudentsNumberReasons = textBox_ChangeStudentsNumberReasons.Text;

            foreach (DataGridViewRow row in dataGridView_UnratedStudents.Rows)
            {
                string name = (row.Cells[0].Value is null) ? "" : row.Cells[0].Value.ToString();
                string reason = (row.Cells[1].Value is null) ? "" : row.Cells[1].Value.ToString();
                if (name == "" && reason == "")
                {
                    continue;
                }
                FirstPageData.UnratedStudents.Add(new SingledOutStudent(name, reason));
            }
        }
        private void HandleWrongStudentsNumber(object sender, EventArgs e)
        {
            if (numericUpDown_NoFailingGradesStudentsNumber.Value > numericUpDown_RatedStudentsNumber.Value + numericUpDown_UnratedStudentsNumber.Value)
            {
                _ = MessageBox.Show("Liczbę uczniów bez ocen ndst. przekracza liczbę wszystkich uczniów.", "Uwaga!");
                NoErrorsOccured = false;
            }
        }
        private void SemesterChanged(object sender, EventArgs e)
        {
            Document.semester = radioButton_Semester1.Checked ? 1 : 2;
            UpdateSemester();
        }
        protected override void UpdateSemester()
        {
            label_NoFailingGradesStudentsNumber.Text = ApplicationTexts.NoFailingGradesStudentsNumberLabelText[radioButton_Semester1.Checked ? 0 : 1];
            label_ChangeStudentsNumberReasons.Enabled = radioButton_Semester1.Checked;
            textBox_ChangeStudentsNumberReasons.Enabled = radioButton_Semester1.Checked;
            label_UnratedStudents.Enabled = radioButton_Semester1.Checked;
            dataGridView_UnratedStudents.Enabled = radioButton_Semester1.Checked;
            dataGridView_UnratedStudents.ColumnHeadersVisible = Document.semester == 1;
        }
    }
}
