﻿
namespace pdf_form_generator
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox_FailingGrades = new System.Windows.Forms.GroupBox();
            this.groupBox_FailingGradesCount = new System.Windows.Forms.GroupBox();
            this.label_OneFailingGrade = new System.Windows.Forms.Label();
            this.numericUpDown_ThreeAndMoreFailingGrades = new System.Windows.Forms.NumericUpDown();
            this.label_TwoFailingGrades = new System.Windows.Forms.Label();
            this.numericUpDown_TwoFailingGrades = new System.Windows.Forms.NumericUpDown();
            this.label_ThreeAndMoreFailingGrades = new System.Windows.Forms.Label();
            this.numericUpDown_OneFailingGrade = new System.Windows.Forms.NumericUpDown();
            this.dataGridView_FailingGradesStudents = new System.Windows.Forms.DataGridView();
            this.Column_FailingGradesStudents_StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_FailingGradesStudents_Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_FailingGradesStudents_ActionPropositions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_FailingGrades.SuspendLayout();
            this.groupBox_FailingGradesCount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ThreeAndMoreFailingGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TwoFailingGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OneFailingGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_FailingGradesStudents)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_FailingGrades
            // 
            this.groupBox_FailingGrades.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_FailingGrades.Controls.Add(this.dataGridView_FailingGradesStudents);
            this.groupBox_FailingGrades.Location = new System.Drawing.Point(11, 11);
            this.groupBox_FailingGrades.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox_FailingGrades.Name = "groupBox_FailingGrades";
            this.groupBox_FailingGrades.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox_FailingGrades.Size = new System.Drawing.Size(762, 485);
            this.groupBox_FailingGrades.TabIndex = 1;
            this.groupBox_FailingGrades.TabStop = false;
            this.groupBox_FailingGrades.Text = "groupBox_TitledStudents";
            // 
            // groupBox_FailingGradesCount
            // 
            this.groupBox_FailingGradesCount.Controls.Add(this.label_OneFailingGrade);
            this.groupBox_FailingGradesCount.Controls.Add(this.numericUpDown_ThreeAndMoreFailingGrades);
            this.groupBox_FailingGradesCount.Controls.Add(this.label_TwoFailingGrades);
            this.groupBox_FailingGradesCount.Controls.Add(this.numericUpDown_TwoFailingGrades);
            this.groupBox_FailingGradesCount.Controls.Add(this.label_ThreeAndMoreFailingGrades);
            this.groupBox_FailingGradesCount.Controls.Add(this.numericUpDown_OneFailingGrade);
            this.groupBox_FailingGradesCount.Location = new System.Drawing.Point(100, 501);
            this.groupBox_FailingGradesCount.Name = "groupBox_FailingGradesCount";
            this.groupBox_FailingGradesCount.Size = new System.Drawing.Size(584, 49);
            this.groupBox_FailingGradesCount.TabIndex = 2;
            this.groupBox_FailingGradesCount.TabStop = false;
            this.groupBox_FailingGradesCount.Text = "groupBox_TitledStudents";
            // 
            // label_OneFailingGrade
            // 
            this.label_OneFailingGrade.Location = new System.Drawing.Point(6, 16);
            this.label_OneFailingGrade.Name = "label_OneFailingGrade";
            this.label_OneFailingGrade.Size = new System.Drawing.Size(105, 25);
            this.label_OneFailingGrade.Text = "label1";
            this.label_OneFailingGrade.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_OneFailingGrade
            // 
            this.numericUpDown_OneFailingGrade.Location = new System.Drawing.Point(117, 20);
            this.numericUpDown_OneFailingGrade.Name = "numericUpDown_OneFailingGrade";
            this.numericUpDown_OneFailingGrade.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_OneFailingGrade.TabIndex = 0;
            this.numericUpDown_OneFailingGrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_TwoFailingGrades
            // 
            this.label_TwoFailingGrades.Location = new System.Drawing.Point(183, 16);
            this.label_TwoFailingGrades.Name = "label_TwoFailingGrades";
            this.label_TwoFailingGrades.Size = new System.Drawing.Size(105, 25);
            this.label_TwoFailingGrades.Text = "label2";
            this.label_TwoFailingGrades.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_TwoFailingGrades
            // 
            this.numericUpDown_TwoFailingGrades.Location = new System.Drawing.Point(294, 20);
            this.numericUpDown_TwoFailingGrades.Name = "numericUpDown_TwoFailingGrades";
            this.numericUpDown_TwoFailingGrades.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_TwoFailingGrades.TabIndex = 1;
            this.numericUpDown_TwoFailingGrades.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_ThreeAndMoreFailingGrades
            // 
            this.label_ThreeAndMoreFailingGrades.Location = new System.Drawing.Point(370, 16);
            this.label_ThreeAndMoreFailingGrades.Name = "label_ThreeAndMoreFailingGrades";
            this.label_ThreeAndMoreFailingGrades.Size = new System.Drawing.Size(130, 25);
            this.label_ThreeAndMoreFailingGrades.Text = "label3";
            this.label_ThreeAndMoreFailingGrades.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_ThreeAndMoreFailingGrades
            // 
            this.numericUpDown_ThreeAndMoreFailingGrades.Location = new System.Drawing.Point(506, 20);
            this.numericUpDown_ThreeAndMoreFailingGrades.Name = "numericUpDown_ThreeAndMoreFailingGrades";
            this.numericUpDown_ThreeAndMoreFailingGrades.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown_ThreeAndMoreFailingGrades.TabIndex = 3;
            this.numericUpDown_ThreeAndMoreFailingGrades.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridView_FailingGradesStudents
            // 
            this.dataGridView_FailingGradesStudents.AllowUserToResizeColumns = false;
            this.dataGridView_FailingGradesStudents.AllowUserToResizeRows = false;
            this.dataGridView_FailingGradesStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_FailingGradesStudents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_FailingGradesStudents.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_FailingGradesStudents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_FailingGradesStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_FailingGradesStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_FailingGradesStudents_StudentName,
            this.Column_FailingGradesStudents_Subject,
            this.Column_FailingGradesStudents_ActionPropositions});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_FailingGradesStudents.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_FailingGradesStudents.Location = new System.Drawing.Point(6, 15);
            this.dataGridView_FailingGradesStudents.Name = "dataGridView_FailingGradesStudents";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_FailingGradesStudents.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_FailingGradesStudents.RowHeadersVisible = false;
            this.dataGridView_FailingGradesStudents.RowHeadersWidth = 65;
            this.dataGridView_FailingGradesStudents.Size = new System.Drawing.Size(751, 465);
            this.dataGridView_FailingGradesStudents.TabIndex = 9;
            // 
            // Column_FailingGradesStudents_StudentName
            // 
            this.Column_FailingGradesStudents_StudentName.HeaderText = "Imię i nazwisko";
            this.Column_FailingGradesStudents_StudentName.Name = "Column_FailingGradesStudents_StudentName";
            this.Column_FailingGradesStudents_StudentName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_FailingGradesStudents_StudentName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_FailingGradesStudents_StudentName.Width = 150;
            // 
            // Column_FailingGradesStudents_Subject
            // 
            this.Column_FailingGradesStudents_Subject.HeaderText = "Zajęcia edukacyjne";
            this.Column_FailingGradesStudents_Subject.Name = "Column_FailingGradesStudents_Subject";
            this.Column_FailingGradesStudents_Subject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_FailingGradesStudents_Subject.Width = 130;
            // 
            // Column_FailingGradesStudents_ActionPropositions
            // 
            this.Column_FailingGradesStudents_ActionPropositions.HeaderText = "Propozycje działań";
            this.Column_FailingGradesStudents_ActionPropositions.Name = "Column_FailingGradesStudents_ActionPropositions";
            this.Column_FailingGradesStudents_ActionPropositions.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_FailingGradesStudents_ActionPropositions.Width = 450;
            // 
            // Form2
            // 
            this.Controls.Add(this.groupBox_FailingGradesCount);
            this.Controls.Add(this.groupBox_FailingGrades);
            this.groupBox_FailingGrades.ResumeLayout(false);
            this.groupBox_FailingGrades.PerformLayout();
            this.groupBox_FailingGradesCount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ThreeAndMoreFailingGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TwoFailingGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OneFailingGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_FailingGradesStudents)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_FailingGrades;
        private System.Windows.Forms.GroupBox groupBox_FailingGradesCount;
        private System.Windows.Forms.Label label_OneFailingGrade;
        private System.Windows.Forms.NumericUpDown numericUpDown_ThreeAndMoreFailingGrades;
        private System.Windows.Forms.Label label_TwoFailingGrades;
        private System.Windows.Forms.NumericUpDown numericUpDown_TwoFailingGrades;
        private System.Windows.Forms.Label label_ThreeAndMoreFailingGrades;
        private System.Windows.Forms.NumericUpDown numericUpDown_OneFailingGrade;
        private System.Windows.Forms.DataGridView dataGridView_FailingGradesStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_FailingGradesStudents_StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_FailingGradesStudents_Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_FailingGradesStudents_ActionPropositions;
    }
}