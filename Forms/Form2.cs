﻿using System.Windows.Forms;
using System.Collections.Generic;

namespace pdf_form_generator
{
    public partial class Form2 : NewForm
    {
        public Form2(NewForm previousForm) : base(previousForm)
        {
            InitializeComponent();
            WriteTexts();

            dataGridView_FailingGradesStudents.CellValueChanged += new DataGridViewCellEventHandler(HandleCellValueChangedEvent);
        }

        private void WriteTexts()
        {
            Text += ApplicationTexts.FormNameSuffixes[1];

            groupBox_FailingGrades.Text = ApplicationTexts.FailingGradesGroupBoxName;
            Column_FailingGradesStudents_StudentName.HeaderText = ApplicationTexts.StudentNameColumnText;
            Column_FailingGradesStudents_Subject.HeaderText = ApplicationTexts.FailingGradesStudentsSubjectColumnText;
            Column_FailingGradesStudents_ActionPropositions.HeaderText = ApplicationTexts.FailingGradesStudentsActionPropositionsColumnText;

            groupBox_FailingGradesCount.Text = ApplicationTexts.FailingGradesCountGroupBoxName;
            label_OneFailingGrade.Text = ApplicationTexts.OneFailingGradeStudentsNumberLabelText;
            label_TwoFailingGrades.Text = ApplicationTexts.TwoFailingGradesStudentsNumberLabelText;
            label_ThreeAndMoreFailingGrades.Text = ApplicationTexts.ThreeAndMoreFailingGradesStudentsNumberLabelText;
        }
        protected override void GatherData()
        {
            foreach (DataGridViewRow row in dataGridView_FailingGradesStudents.Rows)
            {
                string name = (row.Cells[0].Value is null) ? "" : row.Cells[0].Value.ToString();
                string subject = (row.Cells[1].Value is null) ? "" : row.Cells[1].Value.ToString();
                string actionPropositon = (row.Cells[2].Value is null) ? "" : row.Cells[2].Value.ToString();
                if (name == "" && subject == "" && actionPropositon == "")
                {
                    continue;
                }
                FirstPageData.FailingGradeStudents.Add(new FailingGradeStudent(name, subject, actionPropositon));
            }

            FirstPageData.OneFailingGradeStudentsNumber = numericUpDown_OneFailingGrade.Value;
            FirstPageData.TwoFailingGradesStudentsNumber = numericUpDown_TwoFailingGrades.Value;
            FirstPageData.ThreeAndMoreFailingGradesStudentsNumber = numericUpDown_ThreeAndMoreFailingGrades.Value;
        }
        protected override void UpdateSemester()
        {
            groupBox_FailingGrades.Text = ApplicationTexts.FailingGradesGroupBoxName;
            Column_FailingGradesStudents_ActionPropositions.HeaderText = ApplicationTexts.FailingGradesStudentsActionPropositionsColumnText;
            Column_FailingGradesStudents_ActionPropositions.ReadOnly = Document.semester == 2;
        }
        private void HandleCellValueChangedEvent(object sender, DataGridViewCellEventArgs e)
        {
            if (Document.semester == 2)
            {
                if (e.ColumnIndex == 0)
                {
                    string name = dataGridView_FailingGradesStudents.Rows[e.RowIndex].Cells[0].Value.ToString();
                    List<int> rowsIndices = new List<int>();
                    for (int i = 0; i < dataGridView_FailingGradesStudents.Rows.Count; i++)
                    {
                        if (dataGridView_FailingGradesStudents.Rows[i].Cells[0].Value != null && dataGridView_FailingGradesStudents.Rows[i].Cells[0].Value.ToString() == name)
                        {
                            rowsIndices.Add(i);
                        }
                    }
                    foreach (int rowIndex in rowsIndices)
                    {
                        dataGridView_FailingGradesStudents.Rows[rowIndex].Cells[2].Value = rowsIndices.Count;
                    }
                }
            }
        }
    }
}
