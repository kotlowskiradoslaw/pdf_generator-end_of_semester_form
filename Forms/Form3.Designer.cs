﻿
namespace pdf_form_generator
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox_BehaviourGrades = new System.Windows.Forms.GroupBox();
            this.numericUpDown_BehaviourGradesCount6 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount6 = new System.Windows.Forms.Label();
            this.numericUpDown_BehaviourGradesCount5 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount5 = new System.Windows.Forms.Label();
            this.numericUpDown_BehaviourGradesCount4 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount4 = new System.Windows.Forms.Label();
            this.numericUpDown_BehaviourGradesCount3 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount3 = new System.Windows.Forms.Label();
            this.numericUpDown_BehaviourGradesCount2 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount2 = new System.Windows.Forms.Label();
            this.numericUpDown_BehaviourGradesCount1 = new System.Windows.Forms.NumericUpDown();
            this.label_BehaviourGradesCount1 = new System.Windows.Forms.Label();
            this.groupBox_ReprehensibleStudents = new System.Windows.Forms.GroupBox();
            this.dataGridView_ReprehensibleStudents = new System.Windows.Forms.DataGridView();
            this.groupBox_PunishedStudents = new System.Windows.Forms.GroupBox();
            this.Column_ReprehensibleStudents_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_ReprehensibleStudents_Reasons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_TitledStudents = new System.Windows.Forms.GroupBox();
            this.dataGridView_TitledStudents = new System.Windows.Forms.DataGridView();
            this.Column_TitledStudents_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_TitledStudents_Reasons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView_PunishedStudents = new System.Windows.Forms.DataGridView();
            this.Column_PunishedStudents_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_PunishedStudents_Punishment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_PunishedStudents_Reasons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_BehaviourGrades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount1)).BeginInit();
            this.groupBox_ReprehensibleStudents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ReprehensibleStudents)).BeginInit();
            this.groupBox_PunishedStudents.SuspendLayout();
            this.groupBox_TitledStudents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_TitledStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PunishedStudents)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_BehaviourGrades
            // 
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount6);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount6);
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount5);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount5);
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount4);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount4);
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount3);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount3);
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount2);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount2);
            this.groupBox_BehaviourGrades.Controls.Add(this.numericUpDown_BehaviourGradesCount1);
            this.groupBox_BehaviourGrades.Controls.Add(this.label_BehaviourGradesCount1);
            this.groupBox_BehaviourGrades.Location = new System.Drawing.Point(12, 12);
            this.groupBox_BehaviourGrades.Name = "groupBox_BehaviourGrades";
            this.groupBox_BehaviourGrades.Size = new System.Drawing.Size(760, 45);
            this.groupBox_BehaviourGrades.TabIndex = 1;
            this.groupBox_BehaviourGrades.TabStop = false;
            this.groupBox_BehaviourGrades.Text = "groupBox_TitledStudents";
            // 
            // numericUpDown_BehaviourGradesCount6
            // 
            this.numericUpDown_BehaviourGradesCount6.Location = new System.Drawing.Point(697, 18);
            this.numericUpDown_BehaviourGradesCount6.Name = "numericUpDown_BehaviourGradesCount6";
            this.numericUpDown_BehaviourGradesCount6.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount6.TabIndex = 5;
            // 
            // label_BehaviourGradesCount6
            // 
            this.label_BehaviourGradesCount6.Location = new System.Drawing.Point(621, 16);
            this.label_BehaviourGradesCount6.Name = "label_BehaviourGradesCount6";
            this.label_BehaviourGradesCount6.Size = new System.Drawing.Size(70, 20);
            this.label_BehaviourGradesCount6.TabIndex = 6;
            this.label_BehaviourGradesCount6.Text = "label1";
            this.label_BehaviourGradesCount6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_BehaviourGradesCount5
            // 
            this.numericUpDown_BehaviourGradesCount5.Location = new System.Drawing.Point(580, 18);
            this.numericUpDown_BehaviourGradesCount5.Name = "numericUpDown_BehaviourGradesCount5";
            this.numericUpDown_BehaviourGradesCount5.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount5.TabIndex = 4;
            // 
            // label_BehaviourGradesCount5
            // 
            this.label_BehaviourGradesCount5.Location = new System.Drawing.Point(479, 16);
            this.label_BehaviourGradesCount5.Name = "label_BehaviourGradesCount5";
            this.label_BehaviourGradesCount5.Size = new System.Drawing.Size(95, 20);
            this.label_BehaviourGradesCount5.TabIndex = 7;
            this.label_BehaviourGradesCount5.Text = "label1";
            this.label_BehaviourGradesCount5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_BehaviourGradesCount4
            // 
            this.numericUpDown_BehaviourGradesCount4.Location = new System.Drawing.Point(438, 18);
            this.numericUpDown_BehaviourGradesCount4.Name = "numericUpDown_BehaviourGradesCount4";
            this.numericUpDown_BehaviourGradesCount4.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount4.TabIndex = 3;
            // 
            // label_BehaviourGradesCount4
            // 
            this.label_BehaviourGradesCount4.Location = new System.Drawing.Point(362, 16);
            this.label_BehaviourGradesCount4.Name = "label_BehaviourGradesCount4";
            this.label_BehaviourGradesCount4.Size = new System.Drawing.Size(70, 20);
            this.label_BehaviourGradesCount4.TabIndex = 8;
            this.label_BehaviourGradesCount4.Text = "label1";
            this.label_BehaviourGradesCount4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_BehaviourGradesCount3
            // 
            this.numericUpDown_BehaviourGradesCount3.Location = new System.Drawing.Point(321, 18);
            this.numericUpDown_BehaviourGradesCount3.Name = "numericUpDown_BehaviourGradesCount3";
            this.numericUpDown_BehaviourGradesCount3.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount3.TabIndex = 2;
            // 
            // label_BehaviourGradesCount3
            // 
            this.label_BehaviourGradesCount3.Location = new System.Drawing.Point(255, 16);
            this.label_BehaviourGradesCount3.Name = "label_BehaviourGradesCount3";
            this.label_BehaviourGradesCount3.Size = new System.Drawing.Size(60, 20);
            this.label_BehaviourGradesCount3.TabIndex = 9;
            this.label_BehaviourGradesCount3.Text = "label1";
            this.label_BehaviourGradesCount3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_BehaviourGradesCount2
            // 
            this.numericUpDown_BehaviourGradesCount2.Location = new System.Drawing.Point(214, 18);
            this.numericUpDown_BehaviourGradesCount2.Name = "numericUpDown_BehaviourGradesCount2";
            this.numericUpDown_BehaviourGradesCount2.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount2.TabIndex = 1;
            // 
            // label_BehaviourGradesCount2
            // 
            this.label_BehaviourGradesCount2.Location = new System.Drawing.Point(123, 16);
            this.label_BehaviourGradesCount2.Name = "label_BehaviourGradesCount2";
            this.label_BehaviourGradesCount2.Size = new System.Drawing.Size(85, 20);
            this.label_BehaviourGradesCount2.TabIndex = 10;
            this.label_BehaviourGradesCount2.Text = "label1";
            this.label_BehaviourGradesCount2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_BehaviourGradesCount1
            // 
            this.numericUpDown_BehaviourGradesCount1.Location = new System.Drawing.Point(82, 18);
            this.numericUpDown_BehaviourGradesCount1.Name = "numericUpDown_BehaviourGradesCount1";
            this.numericUpDown_BehaviourGradesCount1.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown_BehaviourGradesCount1.TabIndex = 0;
            // 
            // label_BehaviourGradesCount1
            // 
            this.label_BehaviourGradesCount1.Location = new System.Drawing.Point(6, 16);
            this.label_BehaviourGradesCount1.Name = "label_BehaviourGradesCount1";
            this.label_BehaviourGradesCount1.Size = new System.Drawing.Size(70, 20);
            this.label_BehaviourGradesCount1.TabIndex = 11;
            this.label_BehaviourGradesCount1.Text = "label1";
            this.label_BehaviourGradesCount1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox_ReprehensibleStudents
            // 
            this.groupBox_ReprehensibleStudents.Controls.Add(this.dataGridView_ReprehensibleStudents);
            this.groupBox_ReprehensibleStudents.Location = new System.Drawing.Point(12, 63);
            this.groupBox_ReprehensibleStudents.Name = "groupBox_ReprehensibleStudents";
            this.groupBox_ReprehensibleStudents.Size = new System.Drawing.Size(760, 125);
            this.groupBox_ReprehensibleStudents.TabIndex = 2;
            this.groupBox_ReprehensibleStudents.TabStop = false;
            this.groupBox_ReprehensibleStudents.Text = "groupBox_TitledStudents";
            // 
            // dataGridView_ReprehensibleStudents
            // 
            this.dataGridView_ReprehensibleStudents.AllowUserToResizeColumns = false;
            this.dataGridView_ReprehensibleStudents.AllowUserToResizeRows = false;
            this.dataGridView_ReprehensibleStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_ReprehensibleStudents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_ReprehensibleStudents.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ReprehensibleStudents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView_ReprehensibleStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ReprehensibleStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_ReprehensibleStudents_Name,
            this.Column_ReprehensibleStudents_Reasons});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_ReprehensibleStudents.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView_ReprehensibleStudents.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_ReprehensibleStudents.Name = "dataGridView_ReprehensibleStudents";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ReprehensibleStudents.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView_ReprehensibleStudents.RowHeadersVisible = false;
            this.dataGridView_ReprehensibleStudents.RowHeadersWidth = 65;
            this.dataGridView_ReprehensibleStudents.Size = new System.Drawing.Size(747, 99);
            this.dataGridView_ReprehensibleStudents.TabIndex = 0;
            // 
            // groupBox_PunishedStudents
            // 
            this.groupBox_PunishedStudents.Controls.Add(this.dataGridView_PunishedStudents);
            this.groupBox_PunishedStudents.Location = new System.Drawing.Point(12, 422);
            this.groupBox_PunishedStudents.Name = "groupBox_PunishedStudents";
            this.groupBox_PunishedStudents.Size = new System.Drawing.Size(760, 100);
            this.groupBox_PunishedStudents.TabIndex = 4;
            this.groupBox_PunishedStudents.TabStop = false;
            this.groupBox_PunishedStudents.Text = "groupBox2";
            // 
            // Column_ReprehensibleStudents_Name
            // 
            this.Column_ReprehensibleStudents_Name.HeaderText = "Column1";
            this.Column_ReprehensibleStudents_Name.Name = "Column_ReprehensibleStudents_Name";
            this.Column_ReprehensibleStudents_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_ReprehensibleStudents_Name.Width = 150;
            // 
            // Column_ReprehensibleStudents_Reasons
            // 
            this.Column_ReprehensibleStudents_Reasons.HeaderText = "Column1";
            this.Column_ReprehensibleStudents_Reasons.Name = "Column_ReprehensibleStudents_Reasons";
            this.Column_ReprehensibleStudents_Reasons.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_ReprehensibleStudents_Reasons.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_ReprehensibleStudents_Reasons.Width = 575;
            // 
            // groupBox_TitledStudents
            // 
            this.groupBox_TitledStudents.Controls.Add(this.dataGridView_TitledStudents);
            this.groupBox_TitledStudents.Location = new System.Drawing.Point(12, 194);
            this.groupBox_TitledStudents.Name = "groupBox_TitledStudents";
            this.groupBox_TitledStudents.Size = new System.Drawing.Size(760, 222);
            this.groupBox_TitledStudents.TabIndex = 3;
            this.groupBox_TitledStudents.TabStop = false;
            this.groupBox_TitledStudents.Text = "groupBox_TitledStudents";
            // 
            // dataGridView_TitledStudents
            // 
            this.dataGridView_TitledStudents.AllowUserToResizeColumns = false;
            this.dataGridView_TitledStudents.AllowUserToResizeRows = false;
            this.dataGridView_TitledStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_TitledStudents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_TitledStudents.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_TitledStudents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView_TitledStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_TitledStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_TitledStudents_Name,
            this.Column_TitledStudents_Reasons});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_TitledStudents.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView_TitledStudents.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_TitledStudents.Name = "dataGridView_TitledStudents";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_TitledStudents.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView_TitledStudents.RowHeadersVisible = false;
            this.dataGridView_TitledStudents.RowHeadersWidth = 65;
            this.dataGridView_TitledStudents.Size = new System.Drawing.Size(747, 197);
            this.dataGridView_TitledStudents.TabIndex = 1;
            // 
            // Column_TitledStudents_Name
            // 
            this.Column_TitledStudents_Name.HeaderText = "Column1";
            this.Column_TitledStudents_Name.Name = "Column_TitledStudents_Name";
            this.Column_TitledStudents_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_TitledStudents_Name.Width = 150;
            // 
            // Column_TitledStudents_Reasons
            // 
            this.Column_TitledStudents_Reasons.HeaderText = "Column1";
            this.Column_TitledStudents_Reasons.Name = "Column_TitledStudents_Reasons";
            this.Column_TitledStudents_Reasons.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_TitledStudents_Reasons.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_TitledStudents_Reasons.Width = 575;
            // 
            // dataGridView_PunishedStudents
            // 
            this.dataGridView_PunishedStudents.AllowUserToResizeColumns = false;
            this.dataGridView_PunishedStudents.AllowUserToResizeRows = false;
            this.dataGridView_PunishedStudents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_PunishedStudents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_PunishedStudents.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_PunishedStudents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridView_PunishedStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_PunishedStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_PunishedStudents_Name,
            this.Column_PunishedStudents_Punishment,
            this.Column_PunishedStudents_Reasons});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_PunishedStudents.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridView_PunishedStudents.Location = new System.Drawing.Point(7, 19);
            this.dataGridView_PunishedStudents.Name = "dataGridView_PunishedStudents";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_PunishedStudents.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView_PunishedStudents.RowHeadersVisible = false;
            this.dataGridView_PunishedStudents.RowHeadersWidth = 65;
            this.dataGridView_PunishedStudents.Size = new System.Drawing.Size(747, 75);
            this.dataGridView_PunishedStudents.TabIndex = 2;
            // 
            // Column_PunishedStudents_Name
            // 
            this.Column_PunishedStudents_Name.HeaderText = "Column1";
            this.Column_PunishedStudents_Name.Name = "Column_PunishedStudents_Name";
            this.Column_PunishedStudents_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_PunishedStudents_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_PunishedStudents_Name.Width = 150;
            // 
            // Column_PunishedStudents_Punishment
            // 
            this.Column_PunishedStudents_Punishment.HeaderText = "Column1";
            this.Column_PunishedStudents_Punishment.Name = "Column_PunishedStudents_Punishment";
            this.Column_PunishedStudents_Punishment.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_PunishedStudents_Punishment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_PunishedStudents_Punishment.Width = 175;
            // 
            // Column_PunishedStudents_Reasons
            // 
            this.Column_PunishedStudents_Reasons.HeaderText = "Column1";
            this.Column_PunishedStudents_Reasons.Name = "Column_PunishedStudents_Reasons";
            this.Column_PunishedStudents_Reasons.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_PunishedStudents_Reasons.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_PunishedStudents_Reasons.Width = 400;
            // 
            // Form3
            // 
            this.Controls.Add(this.groupBox_TitledStudents);
            this.Controls.Add(this.groupBox_PunishedStudents);
            this.Controls.Add(this.groupBox_ReprehensibleStudents);
            this.Controls.Add(this.groupBox_BehaviourGrades);
            this.groupBox_BehaviourGrades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BehaviourGradesCount1)).EndInit();
            this.groupBox_ReprehensibleStudents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ReprehensibleStudents)).EndInit();
            this.groupBox_PunishedStudents.ResumeLayout(false);
            this.groupBox_TitledStudents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_TitledStudents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PunishedStudents)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_BehaviourGrades;
        private System.Windows.Forms.Label label_BehaviourGradesCount1;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount1;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount6;
        private System.Windows.Forms.Label label_BehaviourGradesCount6;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount5;
        private System.Windows.Forms.Label label_BehaviourGradesCount5;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount4;
        private System.Windows.Forms.Label label_BehaviourGradesCount4;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount3;
        private System.Windows.Forms.Label label_BehaviourGradesCount3;
        private System.Windows.Forms.NumericUpDown numericUpDown_BehaviourGradesCount2;
        private System.Windows.Forms.Label label_BehaviourGradesCount2;
        private System.Windows.Forms.GroupBox groupBox_ReprehensibleStudents;
        private System.Windows.Forms.GroupBox groupBox_PunishedStudents;
        private System.Windows.Forms.DataGridView dataGridView_ReprehensibleStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ReprehensibleStudents_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ReprehensibleStudents_Reasons;
        private System.Windows.Forms.GroupBox groupBox_TitledStudents;
        private System.Windows.Forms.DataGridView dataGridView_TitledStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_TitledStudents_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_TitledStudents_Reasons;
        private System.Windows.Forms.DataGridView dataGridView_PunishedStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_PunishedStudents_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_PunishedStudents_Punishment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_PunishedStudents_Reasons;
    }
}