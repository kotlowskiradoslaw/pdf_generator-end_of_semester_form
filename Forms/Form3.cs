﻿using System.Windows.Forms;

namespace pdf_form_generator
{
    public partial class Form3 : NewForm
    {
        public Form3(NewForm previousForm) : base(previousForm)
        {
            InitializeComponent();
            WriteTexts();
        }

        private void WriteTexts()
        {
            Text += ApplicationTexts.FormNameSuffixes[2];

            groupBox_BehaviourGrades.Text = ApplicationTexts.BehaviourGradesGroupBoxName;
            label_BehaviourGradesCount1.Text = ApplicationTexts.BehaviourGradesLabelTexts[0];
            label_BehaviourGradesCount2.Text = ApplicationTexts.BehaviourGradesLabelTexts[1];
            label_BehaviourGradesCount3.Text = ApplicationTexts.BehaviourGradesLabelTexts[2];
            label_BehaviourGradesCount4.Text = ApplicationTexts.BehaviourGradesLabelTexts[3];
            label_BehaviourGradesCount5.Text = ApplicationTexts.BehaviourGradesLabelTexts[4];
            label_BehaviourGradesCount6.Text = ApplicationTexts.BehaviourGradesLabelTexts[5];

            groupBox_ReprehensibleStudents.Text = ApplicationTexts.ReprehensibleStudentsGroupBoxName;
            Column_ReprehensibleStudents_Name.HeaderText = ApplicationTexts.StudentNameColumnText;
            Column_ReprehensibleStudents_Reasons.HeaderText = ApplicationTexts.ReasonsColumnText;

            groupBox_TitledStudents.Text = ApplicationTexts.TitledStudentsGroupBoxName;
            Column_TitledStudents_Name.HeaderText = ApplicationTexts.StudentNameColumnText;
            Column_TitledStudents_Reasons.HeaderText = ApplicationTexts.TitledStudentsReasonsColumnText;

            UpdateSemester();
            Column_PunishedStudents_Name.HeaderText = ApplicationTexts.StudentNameColumnText;
            Column_PunishedStudents_Reasons.HeaderText = ApplicationTexts.ReasonsColumnText;
        }
        protected override void GatherData()
        {
            SecondPageData.BehaviourGradesStatistics["6"] = numericUpDown_BehaviourGradesCount1.Value;
            SecondPageData.BehaviourGradesStatistics["5"] = numericUpDown_BehaviourGradesCount2.Value;
            SecondPageData.BehaviourGradesStatistics["4"] = numericUpDown_BehaviourGradesCount3.Value;
            SecondPageData.BehaviourGradesStatistics["3"] = numericUpDown_BehaviourGradesCount4.Value;
            SecondPageData.BehaviourGradesStatistics["2"] = numericUpDown_BehaviourGradesCount5.Value;
            SecondPageData.BehaviourGradesStatistics["1"] = numericUpDown_BehaviourGradesCount6.Value;

            foreach (DataGridViewRow row in dataGridView_ReprehensibleStudents.Rows)
            {
                string name = (row.Cells[0].Value is null) ? "" : row.Cells[0].Value.ToString();
                string reason = (row.Cells[1].Value is null) ? "" : row.Cells[1].Value.ToString();
                if (name == "" && reason == "")
                {
                    continue;
                }
                SecondPageData.ReprehensibleStudents.Add(new SingledOutStudent(name, reason));
            }

            foreach (DataGridViewRow row in dataGridView_TitledStudents.Rows)
            {
                string name = (row.Cells[0].Value is null) ? "" : row.Cells[0].Value.ToString();
                string reason = (row.Cells[1].Value is null) ? "" : row.Cells[1].Value.ToString();
                if (name == "" && reason == "")
                {
                    continue;
                }
                SecondPageData.TitledStudents.Add(new SingledOutStudent(name, reason));
            }

            FirstPageData.TitledStudentsNumber = SecondPageData.TitledStudents.Count;

            foreach (DataGridViewRow row in dataGridView_PunishedStudents.Rows)
            {
                string name = (row.Cells[0].Value is null) ? "" : row.Cells[0].Value.ToString();
                string punishment = (row.Cells[1].Value is null) ? "" : row.Cells[1].Value.ToString();
                string reason = (row.Cells[2].Value is null) ? "" : row.Cells[2].Value.ToString();
                if (name == "" && punishment == "" && reason == "")
                {
                    continue;
                }
                SecondPageData.PunishedStudents.Add(new PunishedStudent(name, punishment, reason));
            }
        }
        protected override void UpdateSemester()
        {
            groupBox_PunishedStudents.Text = ApplicationTexts.PunishedStudentsGroupBoxName;
            Column_PunishedStudents_Punishment.HeaderText = ApplicationTexts.PunishedStudentsPunishmentColumnText;
            Column_TitledStudents_Reasons.HeaderText = ApplicationTexts.TitledStudentsReasonsColumnText;

            groupBox_ReprehensibleStudents.Enabled = Document.semester == 1;
            dataGridView_ReprehensibleStudents.ColumnHeadersVisible = Document.semester == 1;
        }
    }
}
