﻿namespace pdf_form_generator
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_EducationResultsStatistics = new System.Windows.Forms.GroupBox();
            this.numericUpDown_SubjectGrades6 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades6 = new System.Windows.Forms.Label();
            this.numericUpDown_SubjectGrades5 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades5 = new System.Windows.Forms.Label();
            this.numericUpDown_SubjectGrades4 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades4 = new System.Windows.Forms.Label();
            this.numericUpDown_SubjectGrades3 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades3 = new System.Windows.Forms.Label();
            this.numericUpDown_SubjectGrades2 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades2 = new System.Windows.Forms.Label();
            this.numericUpDown_SubjectGrades1 = new System.Windows.Forms.NumericUpDown();
            this.label_SubjectGrades1 = new System.Windows.Forms.Label();
            this.groupBox_Attendance = new System.Windows.Forms.GroupBox();
            this.button_NextBack = new System.Windows.Forms.Button();
            this.numericUpDown_AbsentUnfairHoursMonth5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentUnfairHoursMonth4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentUnfairHoursMonth3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentUnfairHoursMonth2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentUnfairHoursMonth1 = new System.Windows.Forms.NumericUpDown();
            this.label_AbsentUnfairHoursNumberAttandance = new System.Windows.Forms.Label();
            this.label_HoursNumberAttandance = new System.Windows.Forms.Label();
            this.numericUpDown_AbsentFairHoursMonth5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentFairHoursMonth4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentFairHoursMonth3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentFairHoursMonth2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentFairHoursMonth1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PresentHoursMonth5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PresentHoursMonth4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PresentHoursMonth3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PresentHoursMonth2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PresentHoursMonth1 = new System.Windows.Forms.NumericUpDown();
            this.label_AbsentFairHoursNumberAttandance = new System.Windows.Forms.Label();
            this.label_PresentHoursNumberAttandance = new System.Windows.Forms.Label();
            this.label_MonthsAttandance5 = new System.Windows.Forms.Label();
            this.label_MonthsAttandance4 = new System.Windows.Forms.Label();
            this.label_MonthsAttandance3 = new System.Windows.Forms.Label();
            this.label_MonthsAttandance2 = new System.Windows.Forms.Label();
            this.label_MonthsAttandance1 = new System.Windows.Forms.Label();
            this.groupBox_Conclusions = new System.Windows.Forms.GroupBox();
            this.textBox_Conclusions = new System.Windows.Forms.TextBox();
            this.numericUpDown_PresentHoursMonth6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentFairHoursMonth6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_AbsentUnfairHoursMonth6 = new System.Windows.Forms.NumericUpDown();
            this.label_MonthsAttandance6 = new System.Windows.Forms.Label();
            this.groupBox_EducationResultsStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades1)).BeginInit();
            this.groupBox_Attendance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth1)).BeginInit();
            this.groupBox_Conclusions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth6)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_EducationResultsStatistics
            // 
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades6);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades6);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades5);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades5);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades4);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades4);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades3);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades3);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades2);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades2);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.numericUpDown_SubjectGrades1);
            this.groupBox_EducationResultsStatistics.Controls.Add(this.label_SubjectGrades1);
            this.groupBox_EducationResultsStatistics.Location = new System.Drawing.Point(12, 12);
            this.groupBox_EducationResultsStatistics.Name = "groupBox_EducationResultsStatistics";
            this.groupBox_EducationResultsStatistics.Size = new System.Drawing.Size(760, 45);
            this.groupBox_EducationResultsStatistics.TabIndex = 0;
            this.groupBox_EducationResultsStatistics.TabStop = false;
            this.groupBox_EducationResultsStatistics.Text = "groupBox1";
            // 
            // numericUpDown_SubjectGrades6
            // 
            this.numericUpDown_SubjectGrades6.Location = new System.Drawing.Point(710, 18);
            this.numericUpDown_SubjectGrades6.Name = "numericUpDown_SubjectGrades6";
            this.numericUpDown_SubjectGrades6.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades6.TabIndex = 11;
            // 
            // label_SubjectGrades6
            // 
            this.label_SubjectGrades6.Location = new System.Drawing.Point(620, 16);
            this.label_SubjectGrades6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades6.Name = "label_SubjectGrades6";
            this.label_SubjectGrades6.Size = new System.Drawing.Size(85, 20);
            this.label_SubjectGrades6.TabIndex = 10;
            this.label_SubjectGrades6.Text = "label6";
            this.label_SubjectGrades6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_SubjectGrades5
            // 
            this.numericUpDown_SubjectGrades5.Location = new System.Drawing.Point(575, 18);
            this.numericUpDown_SubjectGrades5.Name = "numericUpDown_SubjectGrades5";
            this.numericUpDown_SubjectGrades5.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades5.TabIndex = 9;
            // 
            // label_SubjectGrades5
            // 
            this.label_SubjectGrades5.Location = new System.Drawing.Point(485, 16);
            this.label_SubjectGrades5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades5.Name = "label_SubjectGrades5";
            this.label_SubjectGrades5.Size = new System.Drawing.Size(85, 20);
            this.label_SubjectGrades5.TabIndex = 8;
            this.label_SubjectGrades5.Text = "label5";
            this.label_SubjectGrades5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_SubjectGrades4
            // 
            this.numericUpDown_SubjectGrades4.Location = new System.Drawing.Point(440, 18);
            this.numericUpDown_SubjectGrades4.Name = "numericUpDown_SubjectGrades4";
            this.numericUpDown_SubjectGrades4.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades4.TabIndex = 7;
            // 
            // label_SubjectGrades4
            // 
            this.label_SubjectGrades4.Location = new System.Drawing.Point(360, 16);
            this.label_SubjectGrades4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades4.Name = "label_SubjectGrades4";
            this.label_SubjectGrades4.Size = new System.Drawing.Size(75, 20);
            this.label_SubjectGrades4.TabIndex = 6;
            this.label_SubjectGrades4.Text = "label4";
            this.label_SubjectGrades4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_SubjectGrades3
            // 
            this.numericUpDown_SubjectGrades3.Location = new System.Drawing.Point(315, 18);
            this.numericUpDown_SubjectGrades3.Name = "numericUpDown_SubjectGrades3";
            this.numericUpDown_SubjectGrades3.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades3.TabIndex = 5;
            // 
            // label_SubjectGrades3
            // 
            this.label_SubjectGrades3.Location = new System.Drawing.Point(255, 16);
            this.label_SubjectGrades3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades3.Name = "label_SubjectGrades3";
            this.label_SubjectGrades3.Size = new System.Drawing.Size(55, 20);
            this.label_SubjectGrades3.TabIndex = 4;
            this.label_SubjectGrades3.Text = "label3";
            this.label_SubjectGrades3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_SubjectGrades2
            // 
            this.numericUpDown_SubjectGrades2.Location = new System.Drawing.Point(210, 18);
            this.numericUpDown_SubjectGrades2.Name = "numericUpDown_SubjectGrades2";
            this.numericUpDown_SubjectGrades2.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades2.TabIndex = 3;
            // 
            // label_SubjectGrades2
            // 
            this.label_SubjectGrades2.Location = new System.Drawing.Point(130, 16);
            this.label_SubjectGrades2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades2.Name = "label_SubjectGrades2";
            this.label_SubjectGrades2.Size = new System.Drawing.Size(75, 20);
            this.label_SubjectGrades2.TabIndex = 2;
            this.label_SubjectGrades2.Text = "label2";
            this.label_SubjectGrades2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_SubjectGrades1
            // 
            this.numericUpDown_SubjectGrades1.Location = new System.Drawing.Point(85, 18);
            this.numericUpDown_SubjectGrades1.Name = "numericUpDown_SubjectGrades1";
            this.numericUpDown_SubjectGrades1.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown_SubjectGrades1.TabIndex = 1;
            // 
            // label_SubjectGrades1
            // 
            this.label_SubjectGrades1.Location = new System.Drawing.Point(5, 16);
            this.label_SubjectGrades1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_SubjectGrades1.Name = "label_SubjectGrades1";
            this.label_SubjectGrades1.Size = new System.Drawing.Size(75, 20);
            this.label_SubjectGrades1.TabIndex = 0;
            this.label_SubjectGrades1.Text = "label1";
            this.label_SubjectGrades1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox_Attendance
            // 
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance6);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth6);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth6);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth6);
            this.groupBox_Attendance.Controls.Add(this.button_NextBack);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth5);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth4);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth3);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth2);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentUnfairHoursMonth1);
            this.groupBox_Attendance.Controls.Add(this.label_AbsentUnfairHoursNumberAttandance);
            this.groupBox_Attendance.Controls.Add(this.label_HoursNumberAttandance);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth5);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth4);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth3);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth2);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_AbsentFairHoursMonth1);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth5);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth4);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth3);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth2);
            this.groupBox_Attendance.Controls.Add(this.numericUpDown_PresentHoursMonth1);
            this.groupBox_Attendance.Controls.Add(this.label_AbsentFairHoursNumberAttandance);
            this.groupBox_Attendance.Controls.Add(this.label_PresentHoursNumberAttandance);
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance5);
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance4);
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance3);
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance2);
            this.groupBox_Attendance.Controls.Add(this.label_MonthsAttandance1);
            this.groupBox_Attendance.Location = new System.Drawing.Point(12, 63);
            this.groupBox_Attendance.Name = "groupBox_Attendance";
            this.groupBox_Attendance.Size = new System.Drawing.Size(760, 123);
            this.groupBox_Attendance.TabIndex = 1;
            this.groupBox_Attendance.TabStop = false;
            this.groupBox_Attendance.Text = "groupBox1";
            // 
            // button_NextBack
            // 
            this.button_NextBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_NextBack.Location = new System.Drawing.Point(659, 95);
            this.button_NextBack.Name = "button_NextBack";
            this.button_NextBack.Size = new System.Drawing.Size(91, 22);
            this.button_NextBack.TabIndex = 27;
            this.button_NextBack.Text = "Dalej";
            this.button_NextBack.UseVisualStyleBackColor = true;
            this.button_NextBack.Visible = false;
            this.button_NextBack.Click += new System.EventHandler(this.Button_NextBack_Click);
            // 
            // numericUpDown_AbsentUnfairHoursMonth5
            // 
            this.numericUpDown_AbsentUnfairHoursMonth5.Location = new System.Drawing.Point(554, 96);
            this.numericUpDown_AbsentUnfairHoursMonth5.Name = "numericUpDown_AbsentUnfairHoursMonth5";
            this.numericUpDown_AbsentUnfairHoursMonth5.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth5.TabIndex = 24;
            this.numericUpDown_AbsentUnfairHoursMonth5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth5.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentUnfairHoursMonth4
            // 
            this.numericUpDown_AbsentUnfairHoursMonth4.Location = new System.Drawing.Point(453, 96);
            this.numericUpDown_AbsentUnfairHoursMonth4.Name = "numericUpDown_AbsentUnfairHoursMonth4";
            this.numericUpDown_AbsentUnfairHoursMonth4.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth4.TabIndex = 23;
            this.numericUpDown_AbsentUnfairHoursMonth4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth4.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentUnfairHoursMonth3
            // 
            this.numericUpDown_AbsentUnfairHoursMonth3.Location = new System.Drawing.Point(352, 96);
            this.numericUpDown_AbsentUnfairHoursMonth3.Name = "numericUpDown_AbsentUnfairHoursMonth3";
            this.numericUpDown_AbsentUnfairHoursMonth3.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth3.TabIndex = 22;
            this.numericUpDown_AbsentUnfairHoursMonth3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth3.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentUnfairHoursMonth2
            // 
            this.numericUpDown_AbsentUnfairHoursMonth2.Location = new System.Drawing.Point(251, 96);
            this.numericUpDown_AbsentUnfairHoursMonth2.Name = "numericUpDown_AbsentUnfairHoursMonth2";
            this.numericUpDown_AbsentUnfairHoursMonth2.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth2.TabIndex = 21;
            this.numericUpDown_AbsentUnfairHoursMonth2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth2.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentUnfairHoursMonth1
            // 
            this.numericUpDown_AbsentUnfairHoursMonth1.Location = new System.Drawing.Point(150, 96);
            this.numericUpDown_AbsentUnfairHoursMonth1.Name = "numericUpDown_AbsentUnfairHoursMonth1";
            this.numericUpDown_AbsentUnfairHoursMonth1.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth1.TabIndex = 20;
            this.numericUpDown_AbsentUnfairHoursMonth1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth1.ThousandsSeparator = true;
            // 
            // label_AbsentUnfairHoursNumberAttandance
            // 
            this.label_AbsentUnfairHoursNumberAttandance.Location = new System.Drawing.Point(14, 93);
            this.label_AbsentUnfairHoursNumberAttandance.Name = "label_AbsentUnfairHoursNumberAttandance";
            this.label_AbsentUnfairHoursNumberAttandance.Size = new System.Drawing.Size(130, 23);
            this.label_AbsentUnfairHoursNumberAttandance.TabIndex = 20;
            this.label_AbsentUnfairHoursNumberAttandance.Text = "label8";
            this.label_AbsentUnfairHoursNumberAttandance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_HoursNumberAttandance
            // 
            this.label_HoursNumberAttandance.Location = new System.Drawing.Point(14, 18);
            this.label_HoursNumberAttandance.Name = "label_HoursNumberAttandance";
            this.label_HoursNumberAttandance.Size = new System.Drawing.Size(130, 23);
            this.label_HoursNumberAttandance.TabIndex = 20;
            this.label_HoursNumberAttandance.Text = "label9";
            this.label_HoursNumberAttandance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDown_AbsentFairHoursMonth5
            // 
            this.numericUpDown_AbsentFairHoursMonth5.Location = new System.Drawing.Point(554, 70);
            this.numericUpDown_AbsentFairHoursMonth5.Name = "numericUpDown_AbsentFairHoursMonth5";
            this.numericUpDown_AbsentFairHoursMonth5.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth5.TabIndex = 18;
            this.numericUpDown_AbsentFairHoursMonth5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth5.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentFairHoursMonth4
            // 
            this.numericUpDown_AbsentFairHoursMonth4.Location = new System.Drawing.Point(453, 70);
            this.numericUpDown_AbsentFairHoursMonth4.Name = "numericUpDown_AbsentFairHoursMonth4";
            this.numericUpDown_AbsentFairHoursMonth4.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth4.TabIndex = 17;
            this.numericUpDown_AbsentFairHoursMonth4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth4.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentFairHoursMonth3
            // 
            this.numericUpDown_AbsentFairHoursMonth3.Location = new System.Drawing.Point(352, 70);
            this.numericUpDown_AbsentFairHoursMonth3.Name = "numericUpDown_AbsentFairHoursMonth3";
            this.numericUpDown_AbsentFairHoursMonth3.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth3.TabIndex = 16;
            this.numericUpDown_AbsentFairHoursMonth3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth3.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentFairHoursMonth2
            // 
            this.numericUpDown_AbsentFairHoursMonth2.Location = new System.Drawing.Point(251, 70);
            this.numericUpDown_AbsentFairHoursMonth2.Name = "numericUpDown_AbsentFairHoursMonth2";
            this.numericUpDown_AbsentFairHoursMonth2.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth2.TabIndex = 15;
            this.numericUpDown_AbsentFairHoursMonth2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth2.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentFairHoursMonth1
            // 
            this.numericUpDown_AbsentFairHoursMonth1.Location = new System.Drawing.Point(150, 70);
            this.numericUpDown_AbsentFairHoursMonth1.Name = "numericUpDown_AbsentFairHoursMonth1";
            this.numericUpDown_AbsentFairHoursMonth1.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth1.TabIndex = 14;
            this.numericUpDown_AbsentFairHoursMonth1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth1.ThousandsSeparator = true;
            // 
            // numericUpDown_PresentHoursMonth5
            // 
            this.numericUpDown_PresentHoursMonth5.Location = new System.Drawing.Point(554, 44);
            this.numericUpDown_PresentHoursMonth5.Name = "numericUpDown_PresentHoursMonth5";
            this.numericUpDown_PresentHoursMonth5.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth5.TabIndex = 12;
            this.numericUpDown_PresentHoursMonth5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth5.ThousandsSeparator = true;
            // 
            // numericUpDown_PresentHoursMonth4
            // 
            this.numericUpDown_PresentHoursMonth4.Location = new System.Drawing.Point(453, 44);
            this.numericUpDown_PresentHoursMonth4.Name = "numericUpDown_PresentHoursMonth4";
            this.numericUpDown_PresentHoursMonth4.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth4.TabIndex = 11;
            this.numericUpDown_PresentHoursMonth4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth4.ThousandsSeparator = true;
            // 
            // numericUpDown_PresentHoursMonth3
            // 
            this.numericUpDown_PresentHoursMonth3.Location = new System.Drawing.Point(352, 44);
            this.numericUpDown_PresentHoursMonth3.Name = "numericUpDown_PresentHoursMonth3";
            this.numericUpDown_PresentHoursMonth3.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth3.TabIndex = 10;
            this.numericUpDown_PresentHoursMonth3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth3.ThousandsSeparator = true;
            // 
            // numericUpDown_PresentHoursMonth2
            // 
            this.numericUpDown_PresentHoursMonth2.Location = new System.Drawing.Point(251, 44);
            this.numericUpDown_PresentHoursMonth2.Name = "numericUpDown_PresentHoursMonth2";
            this.numericUpDown_PresentHoursMonth2.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth2.TabIndex = 9;
            this.numericUpDown_PresentHoursMonth2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth2.ThousandsSeparator = true;
            // 
            // numericUpDown_PresentHoursMonth1
            // 
            this.numericUpDown_PresentHoursMonth1.Location = new System.Drawing.Point(150, 44);
            this.numericUpDown_PresentHoursMonth1.Name = "numericUpDown_PresentHoursMonth1";
            this.numericUpDown_PresentHoursMonth1.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth1.TabIndex = 8;
            this.numericUpDown_PresentHoursMonth1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth1.ThousandsSeparator = true;
            // 
            // label_AbsentFairHoursNumberAttandance
            // 
            this.label_AbsentFairHoursNumberAttandance.Location = new System.Drawing.Point(14, 67);
            this.label_AbsentFairHoursNumberAttandance.Name = "label_AbsentFairHoursNumberAttandance";
            this.label_AbsentFairHoursNumberAttandance.Size = new System.Drawing.Size(130, 23);
            this.label_AbsentFairHoursNumberAttandance.TabIndex = 7;
            this.label_AbsentFairHoursNumberAttandance.Text = "label8";
            this.label_AbsentFairHoursNumberAttandance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_PresentHoursNumberAttandance
            // 
            this.label_PresentHoursNumberAttandance.Location = new System.Drawing.Point(14, 41);
            this.label_PresentHoursNumberAttandance.Name = "label_PresentHoursNumberAttandance";
            this.label_PresentHoursNumberAttandance.Size = new System.Drawing.Size(130, 23);
            this.label_PresentHoursNumberAttandance.TabIndex = 6;
            this.label_PresentHoursNumberAttandance.Text = "label7";
            this.label_PresentHoursNumberAttandance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_MonthsAttandance5
            // 
            this.label_MonthsAttandance5.Location = new System.Drawing.Point(554, 18);
            this.label_MonthsAttandance5.Name = "label_MonthsAttandance5";
            this.label_MonthsAttandance5.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance5.TabIndex = 4;
            this.label_MonthsAttandance5.Text = "label5";
            this.label_MonthsAttandance5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MonthsAttandance4
            // 
            this.label_MonthsAttandance4.Location = new System.Drawing.Point(453, 18);
            this.label_MonthsAttandance4.Name = "label_MonthsAttandance4";
            this.label_MonthsAttandance4.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance4.TabIndex = 3;
            this.label_MonthsAttandance4.Text = "label4";
            this.label_MonthsAttandance4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MonthsAttandance3
            // 
            this.label_MonthsAttandance3.Location = new System.Drawing.Point(352, 18);
            this.label_MonthsAttandance3.Name = "label_MonthsAttandance3";
            this.label_MonthsAttandance3.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance3.TabIndex = 2;
            this.label_MonthsAttandance3.Text = "label3";
            this.label_MonthsAttandance3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MonthsAttandance2
            // 
            this.label_MonthsAttandance2.Location = new System.Drawing.Point(251, 18);
            this.label_MonthsAttandance2.Name = "label_MonthsAttandance2";
            this.label_MonthsAttandance2.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance2.TabIndex = 1;
            this.label_MonthsAttandance2.Text = "label2";
            this.label_MonthsAttandance2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MonthsAttandance1
            // 
            this.label_MonthsAttandance1.Location = new System.Drawing.Point(150, 18);
            this.label_MonthsAttandance1.Name = "label_MonthsAttandance1";
            this.label_MonthsAttandance1.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance1.TabIndex = 0;
            this.label_MonthsAttandance1.Text = "label1";
            this.label_MonthsAttandance1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox_Conclusions
            // 
            this.groupBox_Conclusions.Controls.Add(this.textBox_Conclusions);
            this.groupBox_Conclusions.Location = new System.Drawing.Point(12, 192);
            this.groupBox_Conclusions.Name = "groupBox_Conclusions";
            this.groupBox_Conclusions.Size = new System.Drawing.Size(760, 330);
            this.groupBox_Conclusions.TabIndex = 2;
            this.groupBox_Conclusions.TabStop = false;
            this.groupBox_Conclusions.Text = "groupBox1";
            // 
            // textBox_Conclusions
            // 
            this.textBox_Conclusions.Location = new System.Drawing.Point(7, 19);
            this.textBox_Conclusions.Multiline = true;
            this.textBox_Conclusions.Name = "textBox_Conclusions";
            this.textBox_Conclusions.Size = new System.Drawing.Size(743, 305);
            this.textBox_Conclusions.TabIndex = 0;
            // 
            // numericUpDown_PresentHoursMonth6
            // 
            this.numericUpDown_PresentHoursMonth6.Location = new System.Drawing.Point(655, 44);
            this.numericUpDown_PresentHoursMonth6.Name = "numericUpDown_PresentHoursMonth6";
            this.numericUpDown_PresentHoursMonth6.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_PresentHoursMonth6.TabIndex = 13;
            this.numericUpDown_PresentHoursMonth6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PresentHoursMonth6.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentFairHoursMonth6
            // 
            this.numericUpDown_AbsentFairHoursMonth6.Location = new System.Drawing.Point(655, 70);
            this.numericUpDown_AbsentFairHoursMonth6.Name = "numericUpDown_AbsentFairHoursMonth6";
            this.numericUpDown_AbsentFairHoursMonth6.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentFairHoursMonth6.TabIndex = 19;
            this.numericUpDown_AbsentFairHoursMonth6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentFairHoursMonth6.ThousandsSeparator = true;
            // 
            // numericUpDown_AbsentUnfairHoursMonth6
            // 
            this.numericUpDown_AbsentUnfairHoursMonth6.Location = new System.Drawing.Point(655, 96);
            this.numericUpDown_AbsentUnfairHoursMonth6.Name = "numericUpDown_AbsentUnfairHoursMonth6";
            this.numericUpDown_AbsentUnfairHoursMonth6.Size = new System.Drawing.Size(95, 20);
            this.numericUpDown_AbsentUnfairHoursMonth6.TabIndex = 25;
            this.numericUpDown_AbsentUnfairHoursMonth6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_AbsentUnfairHoursMonth6.ThousandsSeparator = true;
            // 
            // label_MonthsAttandance6
            // 
            this.label_MonthsAttandance6.Location = new System.Drawing.Point(655, 18);
            this.label_MonthsAttandance6.Name = "label_MonthsAttandance6";
            this.label_MonthsAttandance6.Size = new System.Drawing.Size(95, 23);
            this.label_MonthsAttandance6.TabIndex = 31;
            this.label_MonthsAttandance6.Text = "label5";
            this.label_MonthsAttandance6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form4
            // 
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.groupBox_Conclusions);
            this.Controls.Add(this.groupBox_Attendance);
            this.Controls.Add(this.groupBox_EducationResultsStatistics);
            this.Name = "Form4";
            this.groupBox_EducationResultsStatistics.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SubjectGrades1)).EndInit();
            this.groupBox_Attendance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth1)).EndInit();
            this.groupBox_Conclusions.ResumeLayout(false);
            this.groupBox_Conclusions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PresentHoursMonth6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentFairHoursMonth6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AbsentUnfairHoursMonth6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_EducationResultsStatistics;
        private System.Windows.Forms.GroupBox groupBox_Attendance;
        private System.Windows.Forms.GroupBox groupBox_Conclusions;
        private System.Windows.Forms.Label label_SubjectGrades1;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades1;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades6;
        private System.Windows.Forms.Label label_SubjectGrades6;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades5;
        private System.Windows.Forms.Label label_SubjectGrades5;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades4;
        private System.Windows.Forms.Label label_SubjectGrades4;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades3;
        private System.Windows.Forms.Label label_SubjectGrades3;
        private System.Windows.Forms.NumericUpDown numericUpDown_SubjectGrades2;
        private System.Windows.Forms.Label label_SubjectGrades2;
        private System.Windows.Forms.TextBox textBox_Conclusions;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth5;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth4;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth3;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth2;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth1;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth5;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth4;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth3;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth2;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth1;
        private System.Windows.Forms.Label label_AbsentFairHoursNumberAttandance;
        private System.Windows.Forms.Label label_PresentHoursNumberAttandance;
        private System.Windows.Forms.Label label_MonthsAttandance5;
        private System.Windows.Forms.Label label_MonthsAttandance4;
        private System.Windows.Forms.Label label_MonthsAttandance3;
        private System.Windows.Forms.Label label_MonthsAttandance2;
        private System.Windows.Forms.Label label_MonthsAttandance1;
        private System.Windows.Forms.Label label_HoursNumberAttandance;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth5;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth4;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth3;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth2;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth1;
        private System.Windows.Forms.Label label_AbsentUnfairHoursNumberAttandance;
        private System.Windows.Forms.Button button_NextBack;
        private System.Windows.Forms.Label label_MonthsAttandance6;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentUnfairHoursMonth6;
        private System.Windows.Forms.NumericUpDown numericUpDown_AbsentFairHoursMonth6;
        private System.Windows.Forms.NumericUpDown numericUpDown_PresentHoursMonth6;
    }
}