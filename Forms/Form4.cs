﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace pdf_form_generator
{
    public partial class Form4 : NewForm
    {
        public Form4(NewForm previousForm) : base(previousForm)
        {
            InitializeComponent();
            WriteTexts();

            Activated += new EventHandler((object s, EventArgs e) => UpdateSemester());

            System.Collections.Generic.IEnumerable<NumericUpDown> numericUpDowns = groupBox_EducationResultsStatistics.Controls.OfType<NumericUpDown>();
            foreach (NumericUpDown num in numericUpDowns.Concat(groupBox_Attendance.Controls.OfType<NumericUpDown>()))
            {
                num.Maximum = 99999;
            }
        }

        protected override void UpdateSemester()
        {
            button_NextBack.Visible = Document.semester == 2;
            label_MonthsAttandance6.Visible = Document.semester == 1;
            numericUpDown_PresentHoursMonth6.Visible = Document.semester == 1;
            numericUpDown_AbsentUnfairHoursMonth6.Visible = Document.semester == 1;
            numericUpDown_AbsentFairHoursMonth6.Visible = Document.semester == 1;

            label_MonthsAttandance1.Text = ApplicationTexts.MonthsLabelsText[0];
            label_MonthsAttandance2.Text = ApplicationTexts.MonthsLabelsText[1];
            label_MonthsAttandance3.Text = ApplicationTexts.MonthsLabelsText[2];
            label_MonthsAttandance4.Text = ApplicationTexts.MonthsLabelsText[3];
            label_MonthsAttandance5.Text = ApplicationTexts.MonthsLabelsText[4];
            label_MonthsAttandance6.Text = ApplicationTexts.MonthsLabelsText[5];

            groupBox_Conclusions.Text = ApplicationTexts.ConclusionsGroupBoxName;
        }
        private void WriteTexts()
        {
            Text += ApplicationTexts.FormNameSuffixes[3];

            groupBox_EducationResultsStatistics.Text = ApplicationTexts.EducationResultsStatisticsGroupBoxName;
            label_SubjectGrades1.Text = ApplicationTexts.SubjectGradesLabelTexts[0];
            label_SubjectGrades2.Text = ApplicationTexts.SubjectGradesLabelTexts[1];
            label_SubjectGrades3.Text = ApplicationTexts.SubjectGradesLabelTexts[2];
            label_SubjectGrades4.Text = ApplicationTexts.SubjectGradesLabelTexts[3];
            label_SubjectGrades5.Text = ApplicationTexts.SubjectGradesLabelTexts[4];
            label_SubjectGrades6.Text = ApplicationTexts.SubjectGradesLabelTexts[5];

            groupBox_Attendance.Text = ApplicationTexts.AttendanceGroupBoxName;
            label_HoursNumberAttandance.Text = ApplicationTexts.HoursNumberLabelText;
            label_PresentHoursNumberAttandance.Text = ApplicationTexts.PresentHoursNumberLabelText;
            label_AbsentFairHoursNumberAttandance.Text = ApplicationTexts.AbsentFairHoursNumberLabelText;
            label_AbsentUnfairHoursNumberAttandance.Text = ApplicationTexts.AbsentUnfairHoursNumberLabelText;
        }
        protected override void GatherData()
        {
            SecondPageData.SubjectGradesStatistics["6"] = numericUpDown_SubjectGrades1.Value;
            SecondPageData.SubjectGradesStatistics["5"] = numericUpDown_SubjectGrades2.Value;
            SecondPageData.SubjectGradesStatistics["4"] = numericUpDown_SubjectGrades3.Value;
            SecondPageData.SubjectGradesStatistics["3"] = numericUpDown_SubjectGrades4.Value;
            SecondPageData.SubjectGradesStatistics["2"] = numericUpDown_SubjectGrades5.Value;
            SecondPageData.SubjectGradesStatistics["1"] = numericUpDown_SubjectGrades6.Value;

            if (Document.semester == 1)
            {
                decimal presentHoursNumber = numericUpDown_PresentHoursMonth1.Value +
                    numericUpDown_PresentHoursMonth2.Value +
                    numericUpDown_PresentHoursMonth3.Value +
                    numericUpDown_PresentHoursMonth4.Value +
                    numericUpDown_PresentHoursMonth5.Value +
                    numericUpDown_PresentHoursMonth6.Value;
                SecondPageData.AbsentUnfairHoursNumber = numericUpDown_AbsentUnfairHoursMonth1.Value +
                    numericUpDown_AbsentUnfairHoursMonth2.Value +
                    numericUpDown_AbsentUnfairHoursMonth3.Value +
                    numericUpDown_AbsentUnfairHoursMonth4.Value +
                    numericUpDown_AbsentUnfairHoursMonth5.Value +
                    numericUpDown_AbsentUnfairHoursMonth6.Value;
                SecondPageData.AbsentHoursNumber = SecondPageData.AbsentUnfairHoursNumber +
                    numericUpDown_AbsentFairHoursMonth1.Value +
                    numericUpDown_AbsentFairHoursMonth2.Value +
                    numericUpDown_AbsentFairHoursMonth3.Value +
                    numericUpDown_AbsentFairHoursMonth4.Value +
                    numericUpDown_AbsentFairHoursMonth5.Value +
                    numericUpDown_AbsentFairHoursMonth6.Value;
                SecondPageData.SetAttendancePercentage(presentHoursNumber);
            }

            if (Document.semester == 2)
            {
                GatherAttendanceData(button_NextBack.Text == ApplicationTexts.ButtonNextFormText ? 1 : 2);
                decimal presentHoursNumber = SecondPageData.PresentHoursNumber;
                SecondPageData.AbsentUnfairHoursNumber = SecondPageData.Attendance[0][1].Sum() + (SecondPageData.Attendance[1] == null ? 0 : SecondPageData.Attendance[1][1].Sum());
                SecondPageData.AbsentHoursNumber = SecondPageData.Attendance[0][0].Sum() + (SecondPageData.Attendance[1] == null ? 0 : SecondPageData.Attendance[1][0].Sum());
                SecondPageData.SetAttendancePercentage(presentHoursNumber);
            }

            SecondPageData.FinalConclusions = textBox_Conclusions.Text;
        }
        private void Button_NextBack_Click(object sender, EventArgs e)
        {
            if (button_NextBack.Text == ApplicationTexts.ButtonNextFormText)
            {
                GatherAttendanceData(1);
                WriteMonthsLabels(2);
                SetBackNumericData(2);

                button_NextBack.Text = ApplicationTexts.ButtonPreviousFormText;
            }
            else if (button_NextBack.Text == ApplicationTexts.ButtonPreviousFormText)
            {
                GatherAttendanceData(2);
                WriteMonthsLabels(1);
                SetBackNumericData(1);

                button_NextBack.Text = ApplicationTexts.ButtonNextFormText;
            }
        }
        private void GatherAttendanceData(int semester)
        {
            SecondPageData.Attendance[semester - 1] = new decimal[][] {
                new decimal[] { numericUpDown_AbsentFairHoursMonth1.Value, numericUpDown_AbsentFairHoursMonth2.Value, numericUpDown_AbsentFairHoursMonth3.Value, numericUpDown_AbsentFairHoursMonth4.Value, numericUpDown_AbsentFairHoursMonth5.Value },
                new decimal[] { numericUpDown_AbsentUnfairHoursMonth1.Value, numericUpDown_AbsentUnfairHoursMonth2.Value, numericUpDown_AbsentUnfairHoursMonth3.Value, numericUpDown_AbsentUnfairHoursMonth4.Value, numericUpDown_AbsentUnfairHoursMonth5.Value },
                new decimal[] { numericUpDown_PresentHoursMonth1.Value, numericUpDown_PresentHoursMonth2.Value, numericUpDown_PresentHoursMonth3.Value, numericUpDown_PresentHoursMonth4.Value, numericUpDown_PresentHoursMonth5.Value }
            };
        }
        private void WriteMonthsLabels(int semester)
        {
            label_MonthsAttandance1.Text = ApplicationTexts.MonthsLabelsText[(semester - 1) * 5 + 0];
            label_MonthsAttandance2.Text = ApplicationTexts.MonthsLabelsText[(semester - 1) * 5 + 1];
            label_MonthsAttandance3.Text = ApplicationTexts.MonthsLabelsText[(semester - 1) * 5 + 2];
            label_MonthsAttandance4.Text = ApplicationTexts.MonthsLabelsText[(semester - 1) * 5 + 3];
            label_MonthsAttandance5.Text = ApplicationTexts.MonthsLabelsText[(semester - 1) * 5 + 4];
        }
        private void SetBackNumericData(int semester)
        {
            decimal[] values = new decimal[15];
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = SecondPageData.Attendance[semester - 1] == null ? 0 : SecondPageData.Attendance[semester - 1][i / 5][i % 5];
            }

            numericUpDown_AbsentFairHoursMonth1.Value = values[0];
            numericUpDown_AbsentFairHoursMonth2.Value = values[1];
            numericUpDown_AbsentFairHoursMonth3.Value = values[2];
            numericUpDown_AbsentFairHoursMonth4.Value = values[3];
            numericUpDown_AbsentFairHoursMonth5.Value = values[4];

            numericUpDown_AbsentUnfairHoursMonth1.Value = values[5];
            numericUpDown_AbsentUnfairHoursMonth2.Value = values[6];
            numericUpDown_AbsentUnfairHoursMonth3.Value = values[7];
            numericUpDown_AbsentUnfairHoursMonth4.Value = values[8];
            numericUpDown_AbsentUnfairHoursMonth5.Value = values[9];

            numericUpDown_PresentHoursMonth1.Value = values[10];
            numericUpDown_PresentHoursMonth2.Value = values[11];
            numericUpDown_PresentHoursMonth3.Value = values[12];
            numericUpDown_PresentHoursMonth4.Value = values[13];
            numericUpDown_PresentHoursMonth5.Value = values[14];
        }
    }
}
