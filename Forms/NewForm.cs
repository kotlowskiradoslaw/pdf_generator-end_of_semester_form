﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace pdf_form_generator
{
    public abstract partial class NewForm : Form
    {
        private readonly NewForm PreviousForm;
        private NewForm NextForm;

        public bool NoErrorsOccured = true;

        private readonly SaveFileDialog SaveFileDialog = new SaveFileDialog()
        {
            Title = ApplicationTexts.FolderBrowserDialogText,
            InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
            CheckFileExists = false,
            CheckPathExists = true,
            DefaultExt = "pdf",
            Filter = "PDF document (*.pdf)|*.pdf",
            FilterIndex = 2,
            RestoreDirectory = true
        };

        private readonly Button ButtonPreviousForm = new Button()
        {
            Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
            Location = new Point(11, 529),
            Margin = new Padding(2),
            Name = "button_PreviousForm",
            Size = new Size(84, 21),
            TabIndex = 0,
            Text = ApplicationTexts.ButtonPreviousFormText,
            UseVisualStyleBackColor = true,
        };
        protected readonly Button ButtonNextForm = new Button()
        {
            Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
            Location = new Point(689, 529),
            Margin = new Padding(2),
            Name = "button_NextForm",
            Size = new Size(84, 21),
            TabIndex = 20,
            Text = ApplicationTexts.ButtonGeneratePdfText,
            UseVisualStyleBackColor = true,
        };

        private readonly bool IsFirstForm = true;

        public NewForm(NewForm previousForm = null)
        {
            PreviousForm = previousForm;

            if (PreviousForm != null)
            {
                IsFirstForm = false;
                previousForm.SetNextForm(this);

                Activated += new EventHandler(ErrorsOccuredGoToPreviousForm);
            }

            FormClosed += new FormClosedEventHandler(FormClosedAction);

            ButtonPreviousForm.Visible = !IsFirstForm;
            ButtonPreviousForm.Click += new EventHandler(GoToPreviousForm);

            ButtonNextForm.Click += new EventHandler(Button_GeneratePdf_Click);

            WriteInNewFormData();
            InitializeComponent();
        }
        private void WriteInNewFormData()
        {
            foreach (System.Reflection.FieldInfo field in typeof(NewFormData).GetFields())
            {
                typeof(NewForm).GetProperty(field.Name).SetValue(this, field.GetValue(typeof(NewFormData)));
            }
        }
        private void Button_GeneratePdf_Click(object sender, EventArgs e)
        {
            GatherData();

            SaveFileDialog.FileName = ApplicationTexts.FileName;
            if (SaveFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            string fileDirectory = SaveFileDialog.FileName;

            Document pdf = GeneratePdf();
            string saveFileResult = pdf.SaveWithErrorInfo(fileDirectory);
            if (saveFileResult == "OK")
            {
                Program.OpenFile(fileDirectory);
                Close();
            }
            _ = MessageBox.Show(ApplicationTexts.FileOpenedErrorText);
        }
        private void GoToPreviousForm(object sender, EventArgs e)
        {
            Hide();
            PreviousForm.UpdateSemester();
            PreviousForm.Show();
        }
        private void GoToNextForm(object sender, EventArgs e)
        {
            GatherData();
            Hide();
            NextForm.UpdateSemester();
            NextForm.Show();
            NextForm.Activate();
        }
        private void ErrorsOccuredGoToPreviousForm(object sender, EventArgs e)
        {
            if (!PreviousForm.NoErrorsOccured)
            {
                PreviousForm.NoErrorsOccured = true;
                GoToPreviousForm(sender, e);
            }
        }
        private void SetNextForm(NewForm nextForm)
        {
            NextForm = nextForm;

            ButtonNextForm.Click -= new EventHandler(Button_GeneratePdf_Click);
            ButtonNextForm.Click += new EventHandler(GoToNextForm);
            ButtonNextForm.Text = ApplicationTexts.ButtonNextFormText;
        }
        private void FormClosedAction(object sender, FormClosedEventArgs e)
        {
            if (!IsFirstForm)
            {
                PreviousForm.Close();
            }
        }
        protected abstract void GatherData();
        protected abstract void UpdateSemester();

        private Document GeneratePdf()
        {
            Document pdf = new Document(title: ApplicationTexts.DocumentTitle, pagesNumber: 2);
            pdf.SetHeaderFooter();
            pdf.WriteFirstPage();
            pdf.WriteSecondPage();

            return pdf;
        }
    }
}
