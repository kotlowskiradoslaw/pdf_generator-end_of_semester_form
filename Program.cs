using System.Windows.Forms;
using System.Configuration;

namespace pdf_form_generator
{
    internal static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [System.STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            NewForm form1 = new Form1();
            NewForm form2 = new Form2(form1);
            NewForm form3 = new Form3(form2);
            NewForm _ = new Form4(form3);

            OpenInstruction();
            Application.Run(form1);
        }
        private static void OpenInstruction()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (bool.Parse(config.AppSettings.Settings["FirstRun"].Value))
            {
                config.AppSettings.Settings["FirstRun"].Value = false.ToString();
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");

                OpenFile("manual.pdf");
            }
        }
        public static void OpenFile(string filename)
        {
            System.Diagnostics.Process.Start(filename);
        }
    }
}
