# Pdf form generator for the end of a semester

After every semester at school, a form teacher is obliged to fill the form about students' grades, presence percentage etc.

This application has been created upon teachers' request to improve their efficiency and save time.

The manual in polish has been attached as "manual.pdf".

### Gaps in resources

Resources such as an icon (a logo of the school) has been removed.

### Used packages
* PDFsharp
* PDFsharp-MigraDoc